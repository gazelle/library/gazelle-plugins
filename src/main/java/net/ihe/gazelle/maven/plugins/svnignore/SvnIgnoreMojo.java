package net.ihe.gazelle.maven.plugins.svnignore;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.project.MavenProject;

/**
 * Add property svn:ignore to project, with derived folders/files (target,
 * .project, ...)
 * 
 * @goal svnignore
 * @threadSafe
 */
public class SvnIgnoreMojo extends AbstractMojo {

	/**
	 * The current Maven project
	 * 
	 * @parameter expression="${project}"
	 * @readonly
	 * @required
	 */
	private MavenProject project;

	@Override
	public void execute() throws MojoExecutionException, MojoFailureException {
		try {
			File ignoreList = new File(project.getBasedir(), "svnignore~");
			BufferedWriter writer = new BufferedWriter(new FileWriter(ignoreList));
			writer.write("target");
			writer.newLine();
			writer.write(".project");
			writer.newLine();
			writer.write(".settings");
			writer.newLine();
			writer.write(".classpath");
			writer.newLine();
			writer.write(".svnignore");
			writer.newLine();
			writer.write(".factorypath");
			writer.newLine();
			writer.write("work");
			writer.newLine();
			writer.write("bin");
			writer.newLine();
			writer.write("build");
			writer.newLine();
			writer.close();

			getLog().info("Setting svn:ignore on " + project.getBasedir().getAbsolutePath());

			// svn propset svn:ignore -F ~/bin/svnignore .

			final Process processRm = Runtime.getRuntime().exec(
					new String[] { "svn", "propset", "svn:ignore", "-F", ignoreList.getAbsolutePath(), "." }, null,
					project.getBasedir());
			createLogThreads(processRm);
			processRm.waitFor();
			processRm.destroy();

			ignoreList.delete();
		} catch (IOException e) {
			throw new MojoFailureException("Failed to set svn:ignore");
		} catch (InterruptedException e) {
			throw new MojoFailureException("Failed to set svn:ignore");
		}
	}

	protected void createLogThreads(final Process process) {
		new Thread() {
			public void run() {
				try {
					BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
					String line = "";
					try {
						while ((line = reader.readLine()) != null) {
							getLog().info(line);
						}
					} finally {
						reader.close();
					}
				} catch (IOException ioe) {
					getLog().error("", ioe);
				}
			}
		}.start();

		new Thread() {
			public void run() {
				try {
					BufferedReader reader = new BufferedReader(new InputStreamReader(process.getErrorStream()));
					String line = "";
					try {
						while ((line = reader.readLine()) != null) {
							getLog().error(line);
						}
					} finally {
						reader.close();
					}
				} catch (IOException ioe) {
					getLog().error("", ioe);
				}
			}
		}.start();
	}
}
