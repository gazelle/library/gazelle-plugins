package net.ihe.gazelle.maven.plugins.ramtarget;

import java.io.File;
import java.io.IOException;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.project.MavenProject;

/**
 * Creates target on the RAM drive
 * 
 * @goal create-ramtarget
 * @threadSafe
 */
public class CreateRamTargetMojo extends AbstractRamTargetMojo {

	/**
	 * The current Maven project
	 * 
	 * @parameter expression="${project}"
	 * @readonly
	 * @required
	 */
	private MavenProject project;

	/**
	 * Ram disk root folder
	 * 
	 * @parameter
	 * @required
	 */
	private String ramRootFolder;

	/**
	 * The directory where the generated resource files will be stored. The
	 * directory will be registered as a resource root of the project such that
	 * the generated files will participate in later build phases like packaing.
	 * 
	 * @parameter expression="${project.build.directory}"
	 * @required
	 */
	private File targetDirectory;

	@Override
	public void execute() throws MojoExecutionException, MojoFailureException {
		File ramFolder = getRamFolder(project, ramRootFolder);

		if (!ramFolder.exists()) {
			getLog().info("Creating " + ramFolder.getAbsolutePath());
			ramFolder.mkdirs();
		}

		deleteFolder(targetDirectory);

		try {
			getLog().info("Runing ln -s " + ramFolder.getAbsolutePath() + " " + targetDirectory.getAbsolutePath());
			final Process processLn = Runtime.getRuntime().exec(
					new String[] { "ln", "-s", ramFolder.getAbsolutePath(), targetDirectory.getAbsolutePath() });
			createLogThreads(processLn);
			if (processLn.waitFor() != 0) {
				throw new MojoFailureException("Failed to create target folder on RAM disk");
			}
			processLn.destroy();
		} catch (IOException e) {
			throw new MojoFailureException("Failed to create target folder on RAM disk");
		} catch (InterruptedException e) {
			throw new MojoFailureException("Failed to create target folder on RAM disk");
		}

	}

}
