package net.ihe.gazelle.maven.plugins.messages.xhtml;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.ihe.gazelle.maven.plugins.tools.XMLHandler;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;

public class XMLHandlerTranslator implements XMLHandler {

	// Remove all duplicated spaces
	private static final Pattern PATTERN_SPACES = Pattern.compile("\\s+");

	// Invalid chars at the begin or the end
	private static String STRIP_CHARS_START;
	private static String STRIP_CHARS_END;

	static {
		StringBuilder stripChars = new StringBuilder();
		// control chars
		for (int i = 0; i < 33; i++) {
			stripChars.append((char) i);
		}
		stripChars.append("#*+,-/<=>_|~\\()");
		// incorrect chars
		for (int i = 169; i < 256; i++) {
			stripChars.append((char) i);
		}
		STRIP_CHARS_END = stripChars.toString();
		stripChars.append("!.:;?'");
		STRIP_CHARS_START = stripChars.toString();
	}

	private Set<SpotAttribute> spots;
	private Stack<String> stack;

	private Map<String, String> translations;
	private Map<String, String> reverseTranslations;
	private Map<String, String> relaxedTranslations;
	private Map<String, String> newTranslations;

	private String keyPrefix;

	private List<String> ignoredParamNames;
	private boolean ignoreNextAttributes = false;

	public static void main(String[] args) {
		XMLHandlerTranslator handler = new XMLHandlerTranslator(null, null, null, null, null, null, null);
		handler.processText("&nbsp;&nbsp;Find test by keyword or name");
	}

	public XMLHandlerTranslator(String keyPrefix, Set<SpotAttribute> spots, Map<String, String> translations,
			Map<String, String> reverseTranslations, Map<String, String> relaxedTranslations,
			Map<String, String> newTranslations, List<String> ignoredParamNames) {
		super();
		this.keyPrefix = keyPrefix;
		this.stack = new Stack<String>();
		this.spots = spots;
		this.translations = translations;
		this.reverseTranslations = reverseTranslations;
		this.relaxedTranslations = relaxedTranslations;
		this.newTranslations = newTranslations;
		this.ignoredParamNames = ignoredParamNames;
	}

	@Override
	public String text(int line, int col, String text) {
		String tagName = "";
		if (!stack.empty()) {
			tagName = stack.peek();
			if (tagSupported(tagName)) {
				return processText(text);
			}
		}
		return null;
	}

	private boolean tagSupported(String tagName) {
		if (tagName.equals("script")) {
			return false;
		}
		if (tagName.equals("style")) {
			return false;
		}
		return true;
	}

	private boolean attributeSupported(String tagName, String attributeName) {
		if (ignoreNextAttributes) {
			return false;
		}
		for (SpotAttribute spot : spots) {
			if (spot.supportsTextFromAttribute(tagName, attributeName)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void startElement(String tagName) {
		ignoreNextAttributes = false;
		stack.push(tagName);
	}

	@Override
	public void endElement(String tagName) {
		stack.pop();
	}

	@Override
	public String attribute(int line, int col, String tagName, String lvalue, String rvalue) {
		if ((tagName.equals("ui:param") || tagName.equals("f:param")) && lvalue.equals("name")) {
			if (ignoredParamNames != null && ignoredParamNames.contains(rvalue)) {
				ignoreNextAttributes = true;
			}

		}
		if (attributeSupported(tagName, lvalue)) {
			return processText(rvalue);
		}
		return null;
	}

	public String processText(String input) {
		Set<String> elExpressions = new HashSet<String>();
		elExpressions.addAll(elExpressions(input, "#{", "}"));
		elExpressions.addAll(elExpressions(input, "${", "}"));

		List<String> splittedInput = splitExpressions(input, elExpressions);

		StringBuilder translatedText = new StringBuilder();
		for (String expression : splittedInput) {
			if (shouldProcess(expression)) {
				translateExpression(translatedText, expression);
			} else {
				// translatedText.append(StringEscapeUtils.escapeHtml(expression));
				translatedText.append(expression);
			}
		}
		String result = translatedText.toString();
		if (!input.equals(result)) {
			return result;
		}
		return null;
	}

	public static String cleanExpression(String expression) {
		String stripped = stripExpression(expression);
		stripped = stripped.replaceAll("[\\t\\r\\n\\v\\f]", " ");
		Matcher matcher = PATTERN_SPACES.matcher(stripped);
		if (matcher.find()) {
			stripped = matcher.replaceAll(" ");
		}
		return stripped;
	}

	private static String stripExpression(String expression) {
		String stripped = StringUtils.stripStart(expression, STRIP_CHARS_START);
		stripped = StringUtils.stripEnd(stripped, STRIP_CHARS_END);
		return stripped;
	}

	private void translateExpression(StringBuilder translatedText, String expression) {
		String prefix = "";
		String suffix = "";
		String cleanExpression = expression;

		String stripped = stripExpression(cleanExpression);
		if (stripped.length() != cleanExpression.length()) {
			int start = cleanExpression.indexOf(stripped);
			prefix = cleanExpression.substring(0, start);
			suffix = cleanExpression.substring(start + stripped.length());
			cleanExpression = stripped;
		}

		cleanExpression = cleanExpression.replaceAll("[\\t\\r\\n\\v\\f]", " ");
		Matcher matcher = PATTERN_SPACES.matcher(cleanExpression);
		if (matcher.find()) {
			cleanExpression = matcher.replaceAll(" ");
		}
		cleanExpression = StringEscapeUtils.unescapeHtml(cleanExpression);

		String key = reverseTranslations.get(cleanExpression);
		if (key == null) {
			// key = relaxedTranslations.get(cleanExpression);
			// if (key == null) {
			key = createKey(cleanExpression);
			// }
		}

		translatedText.append(prefix).append("#{messages['" + key + "']}").append(suffix);
	}

	private String createKey(String cleanExpression) {
		String key;
		key = WordUtils.capitalize(cleanExpression).replaceAll("[^a-zA-Z0-9]", "");
		if (key.length() > 48) {
			key = key.substring(0, 48);
		}
		key = keyPrefix + "." + key;
		String baseKey = key;
		int i = 2;
		while (translations.containsKey(key)) {
			key = baseKey + "_" + i;
			i++;
		}

		newTranslations.put(key, cleanExpression);
		translations.put(key, cleanExpression);
		reverseTranslations.put(cleanExpression, key);
		return key;
	}

	public boolean shouldProcess(String text) {
		return text != null && StringUtils.indexOfAny(text.toLowerCase(), "azertyuiopmlkjhgfdsqwxcvbn") >= 0
				&& !text.contains(".seam") && !text.contains(".xhtml") && !text.startsWith("#{")
				&& !text.startsWith("${");
	}

	public Collection<? extends String> elExpressions(String text, String open, String close) {
		String[] elExpressions = StringUtils.substringsBetween(text, open, close);
		List<String> result = new ArrayList<String>();
		if (elExpressions != null) {
			for (String elExpression : elExpressions) {
				result.add(open + elExpression + close);
			}
		}
		return result;
	}

	public List<String> splitExpressions(String text, Set<String> elExpressions) {
		List<String> result = new ArrayList<String>();
		result.add(text);
		for (String elExpression : elExpressions) {
			List<String> tmpResult = new ArrayList<String>();
			for (String string : result) {
				tmpResult.addAll(splitExpression(string, elExpression));
			}
			result = tmpResult;
		}
		return result;
	}

	public static List<String> splitExpression(String text, String elExpression) {
		int startPos = StringUtils.indexOf(text, elExpression);
		ArrayList<String> result = new ArrayList<String>();

		if (startPos == 0) {
			if (text.length() == elExpression.length()) {

				result.add(text);

			} else {

				result.add(elExpression);

				String subText = text.substring(startPos + elExpression.length());
				result.addAll(splitExpression(subText, elExpression));

			}
		} else if (startPos >= 0) {

			if (startPos == text.length() - elExpression.length()) {

				String subText = text.substring(0, startPos);
				result.addAll(splitExpression(subText, elExpression));

				result.add(elExpression);

			} else {

				String subText = text.substring(0, startPos);
				result.addAll(splitExpression(subText, elExpression));

				result.add(elExpression);

				subText = text.substring(startPos + elExpression.length());
				result.addAll(splitExpression(subText, elExpression));

			}

		} else {
			result.add(text);
		}
		return result;
	}

	@Override
	public String getLabel() {
		return "Translating";
	}

	@Override
	public void file(File f) {
		//
	}

}
