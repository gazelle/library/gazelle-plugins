package net.ihe.gazelle.maven.plugins.sql;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

import org.apache.commons.lang.StringUtils;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.DefaultArtifact;
import org.apache.maven.artifact.factory.ArtifactFactory;
import org.apache.maven.artifact.handler.DefaultArtifactHandler;
import org.apache.maven.artifact.metadata.ArtifactMetadataSource;
import org.apache.maven.artifact.resolver.ArtifactCollector;
import org.apache.maven.artifact.resolver.ArtifactNotFoundException;
import org.apache.maven.artifact.resolver.ArtifactResolutionException;
import org.apache.maven.artifact.resolver.filter.ArtifactFilter;
import org.apache.maven.artifact.resolver.filter.ScopeArtifactFilter;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.model.Profile;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.project.MavenProject;
import org.apache.maven.shared.dependency.tree.DependencyNode;
import org.apache.maven.shared.dependency.tree.DependencyTreeBuilder;
import org.apache.maven.shared.dependency.tree.DependencyTreeBuilderException;
import org.apache.maven.shared.dependency.tree.traversal.CollectingDependencyNodeVisitor;
import org.apache.maven.shared.filtering.MavenFileFilter;
import org.apache.maven.shared.filtering.MavenFilteringException;
import org.apache.maven.shared.filtering.MavenResourcesExecution;
import org.codehaus.plexus.util.FileUtils;

/**
 * Aggregate SQL files.
 * 
 * @goal aggregate-sql
 * @phase generate-resources
 * @threadSafe
 */
public class AggregateSqlMojo extends AbstractMojo {

	private static final String DEFAULT_PROFILE = "default";

	/**
	 * This is the list of projects currently slated to be built by Maven.
	 * 
	 * @parameter expression="${reactorProjects}"
	 * @required
	 * @readonly
	 */
	private List projects;

	/**
	 * The current Maven project
	 * 
	 * @parameter expression="${project}"
	 * @readonly
	 * @required
	 */
	private MavenProject project;

	/** @component */
	private org.apache.maven.artifact.resolver.ArtifactResolver resolver;

	/** @parameter default-value="${localRepository}" */
	private org.apache.maven.artifact.repository.ArtifactRepository localRepository;

	/** @parameter default-value="${project.remoteArtifactRepositories}" */
	private java.util.List remoteRepositories;

	/**
	 * @component
	 * @required
	 * @readonly
	 */
	private ArtifactFactory artifactFactory;

	/**
	 * @component
	 * @required
	 * @readonly
	 */
	private ArtifactMetadataSource artifactMetadataSource;

	/**
	 * @component
	 * @required
	 * @readonly
	 */
	private ArtifactCollector artifactCollector;

	/**
	 * @component
	 * @required
	 * @readonly
	 */
	private DependencyTreeBuilder treeBuilder;

	/**
	 * @parameter expression="${session}"
	 * @readonly
	 * @required
	 */
	private MavenSession session;

	/**
	 * @component role="org.apache.maven.shared.filtering.MavenFileFilter"
	 *            role-hint="default"
	 * @required
	 */
	private MavenFileFilter mavenFileFilter;

	/**
	 * Filters (property files) to include during the interpolation of the
	 * pom.xml.
	 * 
	 * @parameter
	 */
	private List filters;

	/**
	 * To escape interpolated value with windows path c:\foo\bar will be
	 * replaced with c:\\foo\\bar
	 * 
	 * @parameter expression="${maven.ear.escapedBackslashesInFilePath}"
	 *            default-value="false"
	 */
	private boolean escapedBackslashesInFilePath;

	/**
	 * Expression preceded with the String won't be interpolated \${foo} will be
	 * replaced with ${foo}
	 * 
	 * @parameter expression="${maven.ear.escapeString}"
	 */
	protected String escapeString;

	/**
	 * The encoding to use when reading source files.
	 * 
	 * @parameter default-value="${project.build.sourceEncoding}"
	 */
	private String sourceEncoding;

	/**
	 * The encoding to use when generating Java source files.
	 * 
	 * @parameter default-value="${project.build.outputEncoding}"
	 */
	private String outputEncoding;

	/**
	 * Directory that resources are copied to during the build.
	 * 
	 * @parameter 
	 *            expression="${project.build.directory}/${project.build.finalName}"
	 * @required
	 */
	private File workDirectory;

	/**
	 * The directory where the import.sql files will be temp stored.
	 * 
	 * @parameter expression=
	 *            "${project.build.directory}/generated-resources/sql-build"
	 * @required
	 */
	private File resourceBuildOutputDirectory;

	private List filterWrappers;

	@Override
	public void execute() throws MojoExecutionException, MojoFailureException {
		if (StringUtils.isEmpty(sourceEncoding)) {
			getLog().info(
					"Source file encoding has not been set, using platform encoding "
							+ System.getProperty("file.encoding") + ", i.e. build is platform dependent!");
			sourceEncoding = System.getProperty("file.encoding");
		}
		if (StringUtils.isEmpty(outputEncoding)) {
			getLog().info(
					"Output file encoding has not been set, using platform encoding "
							+ System.getProperty("file.encoding") + ", i.e. build is platform dependent!");
			outputEncoding = System.getProperty("file.encoding");
		}

		resourceBuildOutputDirectory.mkdirs();

		Set<String> activeProfiles = new HashSet<String>();
		for (Iterator it = projects.iterator(); it.hasNext();) {
			MavenProject project = (MavenProject) it.next();
			addProfiles(project, activeProfiles);
		}
		StringBuilder profiles = new StringBuilder("");
		for (String profile : activeProfiles) {
			if (profiles.length() > 0) {
				profiles.append(",");
			}
			profiles.append(profile);
		}
		getLog().info("Active profiles : " + profiles);

		// Then we retrieve sql files from dependencies
		retrieveAllSqlDependencies();
		// Aggregation in a single import.sql
		aggregateSqls(activeProfiles);
	}

	private void aggregateSqls(Set<String> activeProfiles) throws MojoExecutionException {
		List<File> allFiles = new ArrayList<File>();

		File[] sqlFiles = resourceBuildOutputDirectory.listFiles();
		if (sqlFiles != null) {
			for (File file : sqlFiles) {
				if (file.isFile()) {
					if (isValidSql(file)) {
						allFiles.add(file);
					}
				} else {
					if (activeProfiles.contains(file.getName())) {
						File[] profileFiles = file.listFiles();
						for (File profileFile : profileFiles) {
							if (profileFile.isFile()) {
								if (isValidSql(profileFile)) {
									allFiles.add(profileFile);
								}
							}
						}
					}
				}
			}
		}
		Collections.sort(allFiles, new Comparator<File>() {
			@Override
			public int compare(File o1, File o2) {
				String p1 = o1.getName();
				String p2 = o2.getName();
				return p1.compareTo(p2);
			}
		});

		try {
			String line = null;
			File resourceOutputFile = new File(resourceBuildOutputDirectory, "import.sql");
			if (resourceOutputFile.exists()) {
				resourceOutputFile.delete();
			}
			BufferedWriter writer = new BufferedWriter(new FileWriter(resourceOutputFile));
			for (File file : allFiles) {
				BufferedReader reader = new BufferedReader(new FileReader(file));
				writer.write("-- " + file.getName());
				writer.newLine();
				do {
					line = reader.readLine();
					String lineTrim = StringUtils.trimToNull(line);
					if (lineTrim != null && !lineTrim.startsWith("--")) {
						writer.write(line);
						writer.newLine();
					}
				} while (line != null);
				reader.close();
			}
			writer.close();

			try {
				FileUtils.copyFileToDirectory(resourceOutputFile, workDirectory);
			} catch (IOException e) {
				throw new MojoExecutionException("Unable to copy import.sql to final destination", e);
			}

		} catch (Exception e) {
			throw new MojoExecutionException("Failed to merge files", e);
		}

	}

	private boolean isValidSql(File file) {
		String fileName = file.getName();
		if (fileName.toLowerCase().endsWith(".sql") && !fileName.toLowerCase().equals("import.sql")) {
			return true;
		}
		return false;
	}

	private void addProfiles(MavenProject aProject, Set<String> activeProfiles) {
		List<Profile> profiles = aProject.getActiveProfiles();
		if (profiles != null) {
			for (Profile profile : profiles) {
				if (profile != null && profile.getId() != null) {
					activeProfiles.add(profile.getId());
				}
			}
		}
	}

	private Set<Artifact> getAllDependencies() throws MojoExecutionException {
		Set<Artifact> result = new HashSet<Artifact>();
		try {
			ArtifactFilter artifactFilter = new ScopeArtifactFilter(null);

			DependencyNode rootNode = treeBuilder.buildDependencyTree(project, localRepository, artifactFactory,
					artifactMetadataSource, artifactFilter, artifactCollector);

			CollectingDependencyNodeVisitor visitor = new CollectingDependencyNodeVisitor();

			rootNode.accept(visitor);

			List<DependencyNode> nodes = visitor.getNodes();
			for (DependencyNode dependencyNode : nodes) {
				int state = dependencyNode.getState();
				Artifact artifact = dependencyNode.getArtifact();
				if (state == DependencyNode.INCLUDED) {
					result.add(new SpecialArtifact(artifact));
				}
			}
		} catch (DependencyTreeBuilderException e) {
			throw new MojoExecutionException("Failed to get dependencies", e);
		}
		result.remove(new SpecialArtifact(project.getArtifact()));

		return result;
	}

	private void retrieveAllSqlDependencies() throws MojoExecutionException {
		getLog().info("retrieve all sql files from dependencies");

		Set<Artifact> dependencyArtifacts = getAllDependencies();
		Set<Artifact> unResolved = new HashSet<Artifact>();

		if (dependencyArtifacts == null) {
			getLog().info("project.getDependencyArtifacts() is null!!!");
		} else {
			for (Artifact artifact : dependencyArtifacts) {
				if (artifact.isResolved()) {
					retrieveSQLUsingFlag(artifact);
				} else {
					unResolved.add(artifact);
				}
			}
			if (unResolved.size() > 0) {
				for (Artifact artifact : unResolved) {
					try {
						resolver.resolve(artifact, remoteRepositories, localRepository);
					} catch (ArtifactResolutionException e) {
						throw new MojoExecutionException("Failed to find " + artifact, e);
					} catch (ArtifactNotFoundException e) {
						throw new MojoExecutionException("Failed to find " + artifact, e);
					}
					retrieveSQLUsingFlag(artifact);
				}
			}
		}
	}

	private void retrieveSQLUsingFlag(Artifact artifact) throws MojoExecutionException {
		getLog().debug("Checking " + artifact.toString());

		boolean hasFlag = false;

		File artifactFile = artifact.getFile();
		if (artifactFile == null || !artifactFile.exists()) {
			getLog().info("Unable to process " + artifact);
			return;
		}
		try {
			ZipFile zipFile = new ZipFile(artifactFile);
			Enumeration<? extends ZipEntry> entries = zipFile.entries();
			while (entries.hasMoreElements()) {
				ZipEntry zipEntry = (ZipEntry) entries.nextElement();

				if (!zipEntry.isDirectory()) {
					String zipEntryName = zipEntry.getName();
					int lastIndexOf = zipEntryName.lastIndexOf('/') + 1;
					String fileName = zipEntryName.substring(lastIndexOf);

					if (fileName.equals("has-sql")) {
						hasFlag = true;
					}
				}
			}
		} catch (ZipException e) {
			getLog().error("Unable to process artifact (not a valid zip file) : " + artifactFile);
		} catch (IOException e) {
			getLog().error("Unable to process artifact (not a valid zip file) : " + artifactFile);
		}

		if (hasFlag) {
			Artifact sqlArtifact = new DefaultArtifact(artifact.getGroupId(), artifact.getArtifactId(),
					artifact.getVersionRange(), artifact.getScope(), "zip", "sql", new DefaultArtifactHandler("zip"));
			if (!sqlArtifact.isResolved()) {
				try {
					resolver.resolve(sqlArtifact, remoteRepositories, localRepository);
				} catch (ArtifactResolutionException e) {
					throw new MojoExecutionException("Failed to find " + sqlArtifact, e);
				} catch (ArtifactNotFoundException e) {
					throw new MojoExecutionException("Failed to find " + sqlArtifact, e);
				}
			}
			retrieveSQL(sqlArtifact);
		}
	}

	private void retrieveSQL(Artifact artifact) throws MojoExecutionException {
		getLog().info("Found SQLs in " + artifact.toString());
		File artifactFile = artifact.getFile();
		if (artifactFile == null || !artifactFile.exists()) {
			getLog().info("Unable to process " + artifact);
			return;
		}
		try {
			ZipFile zipFile = new ZipFile(artifactFile);
			Enumeration<? extends ZipEntry> entries = zipFile.entries();
			while (entries.hasMoreElements()) {
				ZipEntry zipEntry = (ZipEntry) entries.nextElement();

				if (!zipEntry.isDirectory()) {
					String zipEntryName = zipEntry.getName();

					String[] split = zipEntryName.split("/");
					String fileName = split[split.length - 1];
					if (fileName.toLowerCase().endsWith(".sql") && !fileName.toLowerCase().equals("import.sql")) {
						getLog().debug("Found " + zipEntryName + " in " + artifact.toString());

						String profile = DEFAULT_PROFILE;
						if (split.length > 1) {
							profile = split[split.length - 2];
							if (profile.equals("classes")) {
								profile = DEFAULT_PROFILE;
							}
							if (profile.equals("sql")) {
								profile = DEFAULT_PROFILE;
							}
						}
						File folder = resourceBuildOutputDirectory;
						if (!profile.equals(DEFAULT_PROFILE)) {
							folder = new File(resourceBuildOutputDirectory, profile);
						}

						File tmpFile = new File(resourceBuildOutputDirectory, "tmp.sql");
						InputStream inputStream = zipFile.getInputStream(zipEntry);
						copyToFile(inputStream, tmpFile);

						File targetSQL = new File(folder, fileName);
						filterSQL(tmpFile, targetSQL);

						tmpFile.delete();
					}
				}
			}
		} catch (ZipException e) {
			getLog().error("Unable to process zip " + artifactFile, e);
		} catch (IOException e) {
			getLog().error("Unable to process zip " + artifactFile, e);
		}
	}

	private void filterSQL(File file, File target) throws MojoExecutionException {
		try {
			target.getParentFile().mkdirs();
			mavenFileFilter.copyFile(file, target, true, getFilterWrappers(), null);
		} catch (MavenFilteringException e) {
			throw new MojoExecutionException("Failed to filter", e);
		}
	}

	private List getFilterWrappers() throws MojoExecutionException {
		if (filterWrappers == null) {
			try {
				MavenResourcesExecution mavenResourcesExecution = new MavenResourcesExecution();
				mavenResourcesExecution.setEscapeString(escapeString);
				filterWrappers = mavenFileFilter.getDefaultFilterWrappers(project, filters,
						escapedBackslashesInFilePath, this.session, mavenResourcesExecution);
			} catch (MavenFilteringException e) {
				getLog().error("fail to build filering wrappers " + e.getMessage());
				throw new MojoExecutionException(e.getMessage(), e);
			}
		}
		return filterWrappers;
	}

	private void copyToFile(InputStream stream, File targetFile) throws IOException {
		BufferedOutputStream fOut = null;
		try {
			targetFile.getParentFile().mkdirs();
			fOut = new BufferedOutputStream(new FileOutputStream(targetFile));
			byte[] buffer = new byte[32 * 1024];
			int bytesRead = 0;
			while ((bytesRead = stream.read(buffer)) != -1) {
				fOut.write(buffer, 0, bytesRead);
			}
		} catch (Exception e) {
			targetFile.delete();
			throw new IOException(e);
		}
		fOut.close();
		stream.close();
	}

}
