package net.ihe.gazelle.maven.plugins.messages.xhtml;

public class SpotAttribute {

	private String tagName;

	private String attributeName;

	public SpotAttribute(String tagName, String attributeName) {
		super();
		this.tagName = tagName;
		this.attributeName = attributeName;
	}

	public boolean supportsTextFromAttribute(String tagName, String attributeName) {
		return this.tagName.equals(tagName) && this.attributeName.equals(attributeName);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((attributeName == null) ? 0 : attributeName.hashCode());
		result = prime * result + ((tagName == null) ? 0 : tagName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SpotAttribute other = (SpotAttribute) obj;
		if (attributeName == null) {
			if (other.attributeName != null)
				return false;
		} else if (!attributeName.equals(other.attributeName))
			return false;
		if (tagName == null) {
			if (other.tagName != null)
				return false;
		} else if (!tagName.equals(other.tagName))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SpotAttribute [tagName=");
		builder.append(tagName);
		builder.append(", attributeName=");
		builder.append(attributeName);
		builder.append("]");
		builder.append("\n").append("spots.add(new SpotAttribute(\"").append(tagName).append("\", \"")
				.append(attributeName).append("\"));");
		return builder.toString();
	}

}
