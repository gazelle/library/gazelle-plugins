package net.ihe.gazelle.maven.plugins.messages.xhtml;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.ihe.gazelle.maven.plugins.tools.XMLHandler;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.maven.plugin.logging.Log;

public class XMLHandlerReverseTranslator implements XMLHandler {

	private static Pattern patternDouble;
	private static Pattern patternSimple;

	static {
		patternDouble = getPattern("\"");
		patternSimple = getPattern("\'");
	}

	private static Pattern getPattern(String messageQuote) {
		String regexp = "#\\{\\s*messages\\s*\\[\\s*" + messageQuote + "([A-Za-z0-9_.]*)" + messageQuote
				+ "\\s*\\]\\s*\\}";
		return Pattern.compile(regexp);
	}

	private Map<String, String> translations;
	private Log log;

	public XMLHandlerReverseTranslator(Map<String, String> translations, Log log) {
		super();
		this.translations = translations;
		this.log = log;
	}

	@Override
	public void file(File f) {
		log.info(f.getAbsolutePath());
	}

	@Override
	public String text(int line, int col, String text) {
		return processText(text, line, col);
	}

	@Override
	public void startElement(String tagName) {
		//
	}

	@Override
	public String attribute(int line, int col, String tagName, String lvalue, String rvalue) {
		return processText(rvalue, line, col);
	}

	@Override
	public void endElement(String tagName) {
		//
	}

	@Override
	public String getLabel() {
		return "Reverse translating";
	}

	public static void main(String[] args) {
		Map<String, String> translations = new HashMap<String, String>();
		Log log = null;
		XMLHandlerReverseTranslator xmlHandlerReverseTranslator = new XMLHandlerReverseTranslator(translations, log);
		System.out.println(xmlHandlerReverseTranslator.processText(
				"ejzeifojzefiojzefioj #{ 	messages[ \'okopkeropgkg\'	]   }", 0, 0));
	}

	private String processText(String text, int line, int col) {
		String result = processText(text, patternDouble, line, col);
		result = processText(result, patternSimple, line, col);
		return result;
	}

	private String processText(String text, Pattern pattern, int line, int col) {
		Matcher matcher = pattern.matcher(text);
		if (matcher.find()) {
			String messagesString = matcher.group(0);
			String messagesKey = matcher.group(1);

			String translation = translations.get(messagesKey);
			if (translation == null) {
				log.error(messagesKey + " is not known!");
				translation = "";
			}
			translation = StringEscapeUtils.escapeHtml(translation);
			return processText(text.replace(messagesString, translation), pattern, line, col);
		}
		return text;
	}
	/*

	private String processText(String text, int line, int col) {
		String result = processText(text, "'", line, col);
		result = processText(result, "\"", line, col);
		return result;
	}

	private String processText(String text, String messageQuote, int line, int col) {
		String result = text;

		while (result.matches(regexp))
			while (result.contains("#{messages['") || result.contains("#{messages[\"")) {
				String completekey = result.substring(result.indexOf("#{messages[") + 11);
				int i;
				i = completekey.indexOf('}');
				completekey = result.substring(0, i);
				if (getIndex(completekey) == -1) {

				} else {
					String tmpResult = result.substring(0, result.indexOf("#{messages["));
					result = result.substring(result.indexOf("#{messages[") + 11);

					i = getIndex(result);
					result = result.substring(i + 1);

					i = getIndex(result);
					String key = result.substring(0, i);

					String translation = translations.get(key);
					if (translation == null) {
						log.error(key + " is not known!");
						// log.error(f.getAbsolutePath() + " - line " + line +
						// " - col "
						// + col);
					}

					i = result.indexOf('}');
					result = result.substring(i + 1);

					tmpResult = tmpResult + StringEscapeUtils.escapeHtml(translation);
					tmpResult = tmpResult + result;

					result = tmpResult;
				}

			}
		return result;
	}

	private int getIndex(String result) {
		int i;
		int i1 = result.indexOf('\'');
		int i2 = result.indexOf('\"');
		if (i1 >= 0 && i2 >= 0) {
			i = Math.min(i1, i2);
		} else {
			i = Math.max(i1, i2);
		}
		return i;
	}
	*/
}
