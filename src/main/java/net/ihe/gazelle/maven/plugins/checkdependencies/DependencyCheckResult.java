package net.ihe.gazelle.maven.plugins.checkdependencies;

public class DependencyCheckResult {
	
	private String group;
	
	private String artifact;
	
	private String type;
	
	private String version;
	
	private String latestVersion;
	
	private Boolean shallBeUpdated = false;
	
	public DependencyCheckResult(String group, String artifact, String type, String version) {
		super();
		this.group = group;
		this.artifact = artifact;
		this.type = type;
		this.version = version;
	}

	public DependencyCheckResult(String group, String artifact, String type, String version, String latestVersion) {
		super();
		this.group = group;
		this.artifact = artifact;
		this.type = type;
		this.version = version;
		this.latestVersion = latestVersion;
	}

	public String getGroup() {
		if (group == null) {
			group = "";
		}
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getArtifact() {
		if (artifact == null) {
			artifact = "";
		}
		return artifact;
	}

	public void setArtifact(String artifact) {
		this.artifact = artifact;
	}

	public String getType() {
		if (type == null) {
			type = "";
		}
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getLatestVersion() {
		return latestVersion;
	}

	public void setLatestVersion(String latestVersion) {
		this.latestVersion = latestVersion;
	}

	public Boolean getShallBeUpdated() {
		return shallBeUpdated;
	}

	public void setShallBeUpdated(Boolean shallBeUpdated) {
		this.shallBeUpdated = shallBeUpdated;
	}
	
	public String shallBeUpdatedRendered() {
		String res = "";
		if (this.getShallBeUpdated()) {
			return "yes";
		}
		return res;
	}

	public void checkIfNeedUpdate() {
		if (this.version != null && this.latestVersion != null) {
			int val = MavenVersionComparison.compare(this.version).with(this.latestVersion);
			if (val<0) {
				this.shallBeUpdated = true;
			}
		}
	}
	
}
