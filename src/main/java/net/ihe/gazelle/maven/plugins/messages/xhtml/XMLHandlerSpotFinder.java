package net.ihe.gazelle.maven.plugins.messages.xhtml;

import java.io.File;
import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

import net.ihe.gazelle.maven.plugins.tools.XMLHandler;

public class XMLHandlerSpotFinder implements XMLHandler {

	private Set<SpotAttribute> spots;
	private Set<SpotAttribute> newSpots;
	private Stack<String> stack;

	public XMLHandlerSpotFinder(Set<SpotAttribute> spots) {
		super();
		this.stack = new Stack<String>();
		this.newSpots = new HashSet<SpotAttribute>();
		this.spots = spots;
		populateKnown();
	}

	private void populateKnown() {
		spots.add(new SpotAttribute("a4j:commandButton", "value"));
		spots.add(new SpotAttribute("a4j:commandButton", "title"));
		spots.add(new SpotAttribute("a4j:commandLink", "value"));
		spots.add(new SpotAttribute("a4j:commandLink", "title"));
		spots.add(new SpotAttribute("a4j:htmlCommandLink", "value"));
		spots.add(new SpotAttribute("a4j:htmlCommandLink", "title"));
		spots.add(new SpotAttribute("e:cell", "value"));
		spots.add(new SpotAttribute("f:selectItem", "itemLabel"));
		spots.add(new SpotAttribute("f:selectItems", "noSelectionLabel"));
		spots.add(new SpotAttribute("h:commandButton", "value"));
		spots.add(new SpotAttribute("h:commandButton", "title"));
		spots.add(new SpotAttribute("h:commandLink", "value"));
		spots.add(new SpotAttribute("h:commandLink", "title"));
		spots.add(new SpotAttribute("h:graphicImage", "alt"));
		spots.add(new SpotAttribute("h:graphicImage", "title"));
		spots.add(new SpotAttribute("h:outputText", "value"));
		spots.add(new SpotAttribute("h:outputLink", "title"));
		spots.add(new SpotAttribute("h:selectBooleanCheckbox", "title"));
		spots.add(new SpotAttribute("rich:column", "sortBy"));
		spots.add(new SpotAttribute("rich:editor", "label"));
		spots.add(new SpotAttribute("rich:listShuttle", "copyAllControlLabel"));
		spots.add(new SpotAttribute("rich:listShuttle", "copyControlLabel"));
		spots.add(new SpotAttribute("rich:listShuttle", "removeAllControlLabel"));
		spots.add(new SpotAttribute("rich:listShuttle", "removeControlLabel"));
		spots.add(new SpotAttribute("rich:listShuttle", "sourceCaptionLabel"));
		spots.add(new SpotAttribute("rich:listShuttle", "targetCaptionLabel"));
		spots.add(new SpotAttribute("rich:panel", "header"));
		spots.add(new SpotAttribute("rich:panelMenuGroup", "label"));
		spots.add(new SpotAttribute("rich:panelMenuItem", "label"));
		spots.add(new SpotAttribute("rich:simpleTogglePanel", "label"));
		spots.add(new SpotAttribute("rich:tab", "label"));
		spots.add(new SpotAttribute("rich:toolTip", "value"));
		spots.add(new SpotAttribute("s:link", "value"));
		spots.add(new SpotAttribute("s:selectItems", "label"));
		spots.add(new SpotAttribute("s:selectItems", "noLabelSelection"));
		spots.add(new SpotAttribute("s:selectItems", "noSelectionLabel"));
		spots.add(new SpotAttribute("ui:param", "value"));
	}

	boolean testValue(String value) {
		return value.contains("messages[");
	}

	public Set<SpotAttribute> getNewSpots() {
		return newSpots;
	}

	@Override
	public String text(int line, int col, String text) {
		return null;
	}

	@Override
	public void startElement(String tagName) {
		stack.push(tagName);
	}

	@Override
	public void endElement(String tagName) {
		stack.pop();
	}

	@Override
	public String attribute(int line, int col, String tagName, String lvalue, String rvalue) {
		if (testValue(rvalue)) {
			SpotAttribute spot = new SpotAttribute(tagName, lvalue);
			if (!spots.contains(spot)) {
				spots.add(spot);
				newSpots.add(spot);
			}
		}
		return null;
	}

	@Override
	public String getLabel() {
		return "Looking for spots for translations in ";
	}

	@Override
	public void file(File f) {
		//
	}
}
