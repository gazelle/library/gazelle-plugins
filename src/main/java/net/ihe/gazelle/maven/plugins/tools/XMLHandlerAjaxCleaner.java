package net.ihe.gazelle.maven.plugins.tools;

import java.io.File;

public class XMLHandlerAjaxCleaner implements XMLHandler {

	private static final String[] functions = {

	"onAjaxRequestComplete()",

	"document.body.style.cursor='default'",

	"document.body.style.cursor='progress'",

	"Richfaces.showModalPanel('panelLoading')",

	"Richfaces.hideModalPanel('panelLoading')",

	"#{rich:component('panelLoading')}.show()",

	"#{rich:component('panelLoading')}.hide()"

	};

	private static final String[] forbidden = {

	"onAjaxRequestComplete",

	"document.body.style.cursor",

	"panelLoading"

	};

	public static void main(String[] args) throws Exception {
		XMLParser.processXHTMLInFolder(new File("/home/glandais/code/maven/gazelle-trunk/gazelle-tm"),
				new XMLHandlerAjaxCleaner(), true);
	}

	private File f;

	@Override
	public void file(File f) {
		this.f = f;
	}

	@Override
	public String text(int line, int col, String text) {
		return null;
	}

	@Override
	public void startElement(String tagName) {

	}

	@Override
	public String attribute(int line, int col, String tagName, String lvalue, String rvalue) {
		String result = rvalue;
		for (String function : functions) {
			result = removeFunction(function, result);
		}
		for (String string : forbidden) {
			if (result.contains(string)) {
				System.out.println("Error : " + f.getAbsolutePath() + " (line:" + line + ",col:" + col + ") : " + result);
			}
		}
		if (!result.equals(rvalue) && !result.isEmpty()) {
			System.out.println(f.getAbsolutePath() + " (line:" + line + ",col:" + col + ") : " + result);
		}
		return result;
	}

	private String removeFunction(String function, String result) {
		String result2 = result;
		result2 = result2.replace("javascript:" + function + ";", "");
		result2 = result2.replace(function + ";", "");
		result2 = result2.replace("javascript:" + function, "");
		result2 = result2.replace(function, "");
		return result2;
	}

	@Override
	public void endElement(String tagName) {

	}

	@Override
	public String getLabel() {
		return "Ajax cleaner";
	}

}
