package net.ihe.gazelle.maven.plugins.hapi;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.util.IOUtil;

import ca.uhn.hl7v2.conf.ProfileException;
import ca.uhn.hl7v2.conf.parser.ProfileParser;
import ca.uhn.hl7v2.conf.spec.RuntimeProfile;
import ca.uhn.hl7v2.sourcegen.conf.GenerateDataTypesEnum;
import ca.uhn.hl7v2.sourcegen.conf.ProfileSourceGenerator;
/**
 * Maven Plugin Mojo for generating HAPI conformance classes from our message profiles
 * 
 * @author <a href="mailto:anne-gaelle@ihe-europe.net">Anne-Gaëlle Bergé</a>
 * @goal confgen
 * @phase generate-sources
 * @requiresDependencyResolution runtime
 * @requiresProject
 * @inheritedByDefault false
 */

public class HL7ConfGenMojo extends AbstractMojo{

	/**
	 * The maven project.
	 * 
	 * @parameter expression="${project}"
	 * @required
	 * @readonly
	 */
	protected MavenProject project;

	/**
	 * The target directory for the generated source
	 * 
	 * @parameter
	 * @required
	 */
	protected String targetDirectory;

	/**
	 * The list of packages to create
	 * 
	 * @parameter
	 * @required
	 */
	protected Package[] packages;


	/**
	 * <p>
	 * Should data types be generated. Valid options are:
	 * </p>
	 * <ul>
	 * <li><b>NONE</b>: Do not generate custom data types, use HAPI's normal
	 * data type classes for the HL7 version that the profile corresponds to
	 * <li><b>SINGLE</b>: Generate a single instance of each data type. In this
	 * case, hapi will generate a custom data type for the first instance of
	 * each type that it finds. So, any customizations that need to be made must
	 * be made in the very first time that a particular data type is used within
	 * a profile.
	 * </ul>
	 * 
	 * @parameter default-value="NONE"
	 */
	protected String generateDataTypes = "NONE";

	protected String fileExt = "java";
	
	/**
     * Should structures be treated as resources
     *
     * @parameter default="false"
     */
    private boolean structuresAsResources = false;
	
	/**
     * The package from which to load the templates
     * 
     * @parameter default="ca.uhn.hl7v2.sourcegen.templates"
     */
	protected String templatePackage = "ca.uhn.hl7v2.sourcegen.templates";
	
	/**
	 * Indicates if we must create the import.sql file gathering the SQL line to INSERT
	 * (mapping between package and profile OID)
	 */
	protected boolean generateSQLFile = true;
	
	/**
	 * The template of the SQL command to execute to update the javaPackage attribute of the HL7Profile
	 * in GazelleHL7v2Validator project
	 */
	protected String sqlCommandTemplate = "UPDATE hl7_profile SET java_package = '%package%.message' WHERE oid = '%oid%';\n";

	@Override
	public void execute() throws MojoExecutionException, MojoFailureException {
		
		GenerateDataTypesEnum genDt;
        try {
            genDt = GenerateDataTypesEnum.valueOf(generateDataTypes);
        } catch (IllegalArgumentException e) {
            throw new MojoExecutionException("Unknown \"generateDataTypes\" value: " + generateDataTypes);
        }
        
        StringBuffer sqlCommand = new StringBuffer();
        
        for (Package currentPackage: packages)
        {
        	if (currentPackage.getProfilesURI() != null)
        	{
        		for (String profileURI : currentPackage.getProfilesURI())
        		{
        			try {
        	        	File profileFile = null;
        	        	if (profileURI != null) {
        	        		profileFile = new File(profileURI);
    	        			generate(genDt, profileFile, currentPackage.getName());
        	        	}
        	        	else
        	        	{
        	        		throw new MojoExecutionException(profileFile + " not found");
        	        	}
        	        } catch (Exception e) {
        	            throw new MojoExecutionException(e.getMessage(), e);
        	        }
        		}
        		if (generateSQLFile)
        		{
	        		for (String oid: currentPackage.getProfilesOID())
	        		{
	        			String command = sqlCommandTemplate.replace("%package%", currentPackage.getName()).replace("%oid%", oid);
	        			sqlCommand.append(command);
	        		}
        		}
        	}
        }
        if (generateSQLFile)
        {
	        File sqlFile = new File(project.getBasedir(), "target/import.sql");
	        FileWriter writer;
			try {
				writer = new FileWriter(sqlFile);
				writer.write(sqlCommand.toString());
		        writer.close();
			} catch (IOException e) {
				getLog().error("Unable to create import.sql file");
			}
	        project.addCompileSourceRoot(targetDirectory);
        }
	}

	protected void generate(GenerateDataTypesEnum genDt, File profileFile, String packageName) 
		throws FileNotFoundException, IOException, ProfileException, Exception 
	{
		FileReader reader = new FileReader(profileFile);
		String profileString = IOUtil.toString(reader);

		ProfileParser profileParser = new ProfileParser(false);
		RuntimeProfile runtimeProfile = profileParser.parse(profileString);

		ProfileSourceGenerator gen = new ProfileSourceGenerator(runtimeProfile, targetDirectory, packageName, genDt, templatePackage, fileExt);
		gen.generate();

		if (!structuresAsResources) {
			getLog().info("Adding path to compile sources: " + targetDirectory);
			project.addCompileSourceRoot(targetDirectory);
		}
	}
}
