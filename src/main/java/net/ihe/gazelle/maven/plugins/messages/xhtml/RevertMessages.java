package net.ihe.gazelle.maven.plugins.messages.xhtml;

import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Properties;

import net.ihe.gazelle.maven.plugins.tools.XMLHandler;
import net.ihe.gazelle.maven.plugins.tools.XMLParser;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;

/**
 * Replace translations! Process all XHTML files and replaces #{messages[]} with
 * the real value (in english)
 * 
 * @goal revert-messages
 * @phase process-resources
 * @threadSafe
 */
public class RevertMessages extends AbstractMojo {

	/**
	 * The directory where the XHTML can be found.
	 * 
	 * @parameter expression="${project.basedir}/src/main/webapp"
	 * @required
	 */
	private File xhtmlInputDirectory;

	/**
	 * The directory where the generated resource files will be stored. The
	 * directory will be registered as a resource root of the project such that
	 * the generated files will participate in later build phases like packaing.
	 * 
	 * @parameter expression=
	 *            "${project.build.directory}/generated-resources/messages-aggregated/messages_en.properties"
	 * @required
	 */
	private File existingMessages;

	/**
	 * @parameter expression= "${simulate}"
	 */
	private boolean simulate = false;

	private Map<String, String> translations;

	@Override
	public void execute() throws MojoExecutionException, MojoFailureException {
		if (!existingMessages.exists()) {
			getLog().info(existingMessages.getAbsolutePath() + " does not exist");
		} else {
			prepareTranslations();
			reverseTranslateFiles();
		}
	}

	private void reverseTranslateFiles() throws MojoFailureException {
		XMLHandler translateHandler = new XMLHandlerReverseTranslator(translations, getLog());
		try {
			XMLParser.processXHTMLInFolder(xhtmlInputDirectory, translateHandler, !simulate);
		} catch (Exception e) {
			throw new MojoFailureException("Failed to translate", e);
		}
	}

	private void prepareTranslations() throws MojoFailureException {
		Properties messages = new Properties();
		try {
			messages.load(new FileInputStream(existingMessages));
		} catch (Exception e) {
			throw new MojoFailureException("Failed to read existing messages", e);
		}
		translations = new HashMap<String, String>();
		safePutAll(translations, messages);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void safePutAll(Map<String, String> target, Properties messages) {
		target.putAll((Hashtable) messages);
	}

}
