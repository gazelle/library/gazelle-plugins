package net.ihe.gazelle.maven.plugins.messages.xhtml;

import java.io.File;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import net.ihe.gazelle.maven.plugins.tools.XMLParser;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.project.MavenProject;

/**
 * Check messages in XHTML files.
 * 
 * @goal check-xhtml-messages
 * @phase process-resources
 * @threadSafe
 */
public class CheckMessages extends AbstractMojo {

	/**
	 * The current Maven project
	 * 
	 * @parameter expression="${project}"
	 * @readonly
	 * @required
	 */
	private MavenProject project;

	/**
	 * The directory where the XHTML can be found.
	 * 
	 * @parameter expression="${project.basedir}/src/main/webapp"
	 * @required
	 */
	private File xhtmlInputDirectory;

	/**
	 * The list of ignored names in ui:param/f:param elements. value attribute for this
	 * element will not be translated
	 * 
	 * @parameter expression= "${ignoredParamNames}"
	 */
	private List<String> ignoredParamNames;

	private boolean isCrowdin;

	@Override
	public void execute() throws MojoExecutionException, MojoFailureException {
		Object messagesMode = project.getProperties().get("messages.mode");
		if ("crowdin".equals(messagesMode)) {
			isCrowdin = true;
		} else {
			isCrowdin = false;
		}

		Set<SpotAttribute> spots = collectSpots();
		checkMissingTranslations(spots);
	}

	private void checkMissingTranslations(Set<SpotAttribute> spots) throws MojoFailureException {
		XMLHandlerCheckMissing translateHandler = new XMLHandlerCheckMissing(spots, getLog(),
				XhtmlMessages.getAllIgnoredParamNames(getLog(), ignoredParamNames));
		try {
			XMLParser.processXHTMLInFolder(xhtmlInputDirectory, translateHandler, false);
		} catch (Exception e) {
			throw new MojoFailureException("Failed to translate", e);
		}
		if (translateHandler.hasMissing()) {
			getLog().warn("Some translations are missing!");
			getLog().warn("For automatic translation, execute in this module : ");
			if (isCrowdin) {
				getLog().warn(
						"mvn crowdin:pull crowdin:aggregate gazelle:xhtml-messages -DkeyPrefix=" + project.getGroupId());
			} else {
				getLog().warn(
						"mvn gazelle:generate-messages gazelle:aggregate-messages gazelle:xhtml-messages -DkeyPrefix="
								+ project.getGroupId());
			}
		}
	}

	private Set<SpotAttribute> collectSpots() throws MojoFailureException {
		Set<SpotAttribute> spots = new HashSet<SpotAttribute>();
		XMLHandlerSpotFinder translateHandler = new XMLHandlerSpotFinder(spots);
		try {
			XMLParser.processXHTMLInFolder(xhtmlInputDirectory, translateHandler, false);
		} catch (Exception e) {
			throw new MojoFailureException("Failed to get message locations", e);
		}
		return spots;
	}

}
