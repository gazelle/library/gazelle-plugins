package net.ihe.gazelle.maven.plugins.sql;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.maven.model.Resource;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.project.MavenProject;

/**
 * Generate flag for sql
 * 
 * @goal generate-sql-flag
 * @phase process-resources
 * @threadSafe
 */
public class GenerateSqlFlag extends AbstractMojo {

	/**
	 * The current Maven project
	 * 
	 * @parameter expression="${project}"
	 * @readonly
	 * @required
	 */
	private MavenProject project;

	@Override
	public void execute() throws MojoExecutionException, MojoFailureException {
		File flagFolder = new File(project.getBuild().getDirectory(), "flag-folder");
		try {
			FileUtils.writeStringToFile(new File(flagFolder, "has-sql"), "");
		} catch (IOException e) {
			throw new MojoFailureException("Failed to create flag file");
		}
		Resource resource = new Resource();
		resource.setDirectory(flagFolder.getAbsolutePath());
		this.project.addResource(resource);
	}

}
