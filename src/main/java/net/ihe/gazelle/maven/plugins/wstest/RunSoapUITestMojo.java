package net.ihe.gazelle.maven.plugins.wstest;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.BasicConfigurator;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.project.MavenProject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eviware.soapui.impl.wsdl.WsdlProject;
import com.eviware.soapui.model.support.PropertiesMap;
import com.eviware.soapui.model.testsuite.ProjectRunContext;
import com.eviware.soapui.model.testsuite.ProjectRunListener;
import com.eviware.soapui.model.testsuite.ProjectRunner;
import com.eviware.soapui.model.testsuite.TestCase;
import com.eviware.soapui.model.testsuite.TestCaseRunner;
import com.eviware.soapui.model.testsuite.TestSuiteRunner;
import com.eviware.soapui.model.testsuite.TestRunner.Status;
import com.eviware.soapui.model.testsuite.TestStepResult;
import com.eviware.soapui.model.testsuite.TestSuite;

/**
 * Executes soapui-project files and stores the result in a log file
 * 
 * @author <a href="mailto:anne-gaelle@ihe-europe.net">Anne-Gaëlle Bergé</a>
 * @goal soapui-tests
 * @phase test
 * @requiresDependencyResolution runtime
 * @requiresProject
 * @inheritedByDefault false
 */
public class RunSoapUITestMojo extends AbstractMojo {

	/**
	 * The maven project.
	 * 
	 * @parameter expression="${project}"
	 * @required
	 * @readonly
	 */
	protected MavenProject project;

	/**
	 * A list of testConfiguration instances to be run
	 * 
	 * @parameter
	 * @required
	 */
	protected TestConfiguration[] testConfigurations;

	@Override
	public void execute() throws MojoExecutionException, MojoFailureException {
		int testsCount = 0;
		int failedCount = 0;
		if ((testConfigurations != null) && (testConfigurations.length > 0)) {
			for (TestConfiguration config : testConfigurations) {
				String[] files = null;
				if (config.getSoapUIProjects() != null) {
					files = config.getSoapUIProjects();
				} else if (config.getSoapUIProjectDirectory() != null) {
					File dir = new File(config.getSoapUIProjectDirectory());
					if (dir.isDirectory()) {
						String[] filenames = dir.list(filter);
						if (filenames != null) {
							List<String> pathToAdd = new ArrayList<String>();
							for (String filename : filenames) {
								pathToAdd.add(config.getSoapUIProjectDirectory() + "/" + filename);
							}
							files = pathToAdd.toArray(new String[0]);
						}
					} else {
						moveLogsToTarget();
						throw new MojoExecutionException(config.getSoapUIProjectDirectory() + " is not a directory");
					}
				} else {
					moveLogsToTarget();
					throw new MojoExecutionException("Either a list of file or a directory shall be specified");
				}
				if ((files != null) && (files.length > 0)) {
					for (String file : files) {
						testsCount++;
						if (!executeTest(config.getServiceEndpoint(), file)) {
							failedCount++;
						}
					}
					getLog().info(testsCount + " test project(s) executed");
					getLog().info(failedCount + " test project(s) reported errors");
				} else {
					moveLogsToTarget();
					throw new MojoExecutionException("No soapui-project file has been found");
				}
			}
		} else {
			getLog().error("One test configuration is required at least");
		}
		moveLogsToTarget();
	}

	private void moveLogsToTarget() {
		File basedir = new File(project.getBasedir().getParent());
		String[] logs = basedir.list(filterLogs);
		if (logs != null) {
			for (String log : logs) {
				File file = new File(basedir, log);
				file.renameTo(new File(basedir + "/target/" + log));
			}
		}
	}

	private static FilenameFilter filter = new FilenameFilter() {
		@Override
		public boolean accept(File dir, String name) {
			return name.endsWith(".xml");
		}
	};

	private static FilenameFilter filterLogs = new FilenameFilter() {
		@Override
		public boolean accept(File dir, String name) {
			return name.endsWith(".log");
		}
	};

	private boolean executeTest(String serviceEndpoint, String soapuiProject) throws MojoExecutionException {
		WsdlProject wsdlProject = null;
		boolean success = false;
		try {
			wsdlProject = new WsdlProject(soapuiProject);
			if (serviceEndpoint != null) {
				wsdlProject.setPropertyValue("serviceEndpoint", serviceEndpoint);
			}
		} catch (Exception e) {
			throw new MojoExecutionException("Unable to create project from file " + soapuiProject, e);
		}
		getLog().info("Processing file " + soapuiProject);
		getLog().info("with service endpoint = " + wsdlProject.getPropertyValue("serviceEndpoint"));
		List<TestSuite> testSuites = wsdlProject.getTestSuiteList();

		for (TestSuite tSuite : testSuites) {

			getLog().info("TestSuite: " + tSuite.getName());
			List<TestCase> testCases = tSuite.getTestCaseList();
			for (TestCase tCase : testCases) {
				TestCaseRunner runner = tCase.run(new PropertiesMap(), false);
				getLog().info("Test case executing in " + runner.getTimeTaken() + " ms");
				for (TestStepResult results : runner.getResults()) {
					if (results.getStatus().toString().equals("OK")) {
						getLog().info(results.getTestStep().getName() + "->" + results.getStatus().toString());
					} else {
						getLog().error(results.getTestStep().getName() + "->" + results.getStatus().toString());
					}
					success = success && results.getStatus().equals(Status.FINISHED);
					if (messgeToDisplay(results)) {
						getLog().error("Reported errors: ");
						for (String message : results.getMessages()) {
							getLog().error(message);
						}
					}
				}
			}
		}
		return success;
	}

	private boolean messgeToDisplay(TestStepResult results) {
		return (results.getMessages() != null) && (results.getMessages().length > 0);
	}

}
