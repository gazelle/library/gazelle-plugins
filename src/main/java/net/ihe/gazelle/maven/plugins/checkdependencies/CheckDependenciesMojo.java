package net.ihe.gazelle.maven.plugins.checkdependencies;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.maven.model.Build;
import org.apache.maven.model.Dependency;
import org.apache.maven.model.DependencyManagement;
import org.apache.maven.model.Plugin;
import org.apache.maven.model.PluginManagement;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.project.MavenProject;

/**
 * Check if there is a dependency in this project which is not up to date 
 * 
 * @goal check-dependencies
 * @threadSafe
 */
public class CheckDependenciesMojo extends AbstractMojo {

	/**
	 * The current Maven project
	 * 
	 * @parameter expression="${project}"
	 * @readonly
	 * @required
	 */
	private MavenProject project;
	
	/**
	 * 
	 * @parameter expression="${check.dependencies.verbose}"
	 */
	private Boolean verbose;
	
	/**
	 * 
	 * @parameter expression="${check.dependencies.all}"
	 */
	private Boolean checkAll;
	
	/**
	 * 
	 * @parameter expression="${check.dependencies.management}"
	 */
	private Boolean checkDM;
	

	private String nexusURL = "https://gazelle.ihe.net/nexus/service/local/";

	@Override
	public void execute() throws MojoExecutionException, MojoFailureException {
		List<DependencyCheckResult> listResult = extractListDependenciesCheckResult();
		showListDependencyResults(listResult);
	}

	public List<DependencyCheckResult> extractListDependenciesCheckResult() {
		List<DependencyCheckResult> listResult = new ArrayList<DependencyCheckResult>();
		checkDependencies(listResult);
		checkDependencyManagement(listResult);
		checkPluginsDependencies(listResult);
		checkPluginManagementDependencies(listResult);
		if (project.hasParent()) {
			checkParentDependency(listResult);
		}
		return listResult;
	}
	
	public MavenProject getProject() {
		return project;
	}

	public void setProject(MavenProject project) {
		this.project = project;
	}

	private void checkPluginManagementDependencies(List<DependencyCheckResult> listResult) {
		if (!getCheckDM()) {
			return;
		}
		Build build = project.getBuild();
		if (build == null) {
			return;
		}
		PluginManagement pm = build.getPluginManagement();
		if (pm == null) {
			return;
		}
		List plugs = pm.getPlugins();
		if (plugs == null) {
			return;
		}
		for (Object object : plugs) {
			if (object instanceof Plugin) {
				Plugin plug = (Plugin)object;
				Dependency dep = new Dependency();
				dep.setArtifactId(plug.getArtifactId());
				dep.setGroupId(plug.getGroupId());
				dep.setVersion(plug.getVersion());
				dep.setType("plugin");
				treatDependency(listResult, dep);
			}
			else {
				System.out.println("Problem to parse a dependency, its type is :" + object.getClass().getName());
			}
		}
	}
	
	private void checkPluginsDependencies(List<DependencyCheckResult> listResult) {
		List plugs = project.getBuildPlugins();
		if (plugs == null) {
			return;
		}
		for (Object object : plugs) {
			if (object instanceof Plugin) {
				Plugin plug = (Plugin)object;
				Dependency dep = new Dependency();
				dep.setArtifactId(plug.getArtifactId());
				dep.setGroupId(plug.getGroupId());
				dep.setVersion(plug.getVersion());
				dep.setType("plugin");
				treatDependency(listResult, dep);
			}
			else {
				System.out.println("Problem to parse a dependency, its type is :" + object.getClass().getName());
			}
		}
	}
	
	private void checkDependencyManagement(List<DependencyCheckResult> listResult) {
		if (!getCheckDM()) {
			return;
		}
		DependencyManagement dm = project.getDependencyManagement();
		if (dm == null) {
			return;
		}
		List ldep = dm.getDependencies();
		if (ldep == null) {
			return;
		}
		for (Object object : ldep) {
			if (object instanceof Dependency) {
				Dependency dep = (Dependency)object;
				treatDependency(listResult, dep);
			}
			else {
				System.out.println("Problem to parse a dependency, its type is :" + object.getClass().getName());
			}
		}
	}
	
	private void checkDependencies(List<DependencyCheckResult> listResult) {
		List ldep = project.getDependencies();
		if (ldep == null || ldep.isEmpty()) {
			return;
		}
		for (Object object : ldep) {
			if (object instanceof Dependency) {
				Dependency dep = (Dependency)object;
				treatDependency(listResult, dep);
			}
			else {
				System.out.println("Problem to parse a dependency, its type is :" + object.getClass().getName());
			}
		}
	}

	private void treatDependency(List<DependencyCheckResult> listResult, Dependency dep) {
		DependencyCheckResult res = new DependencyCheckResult(dep.getGroupId(), dep.getArtifactId(), dep.getType(), dep.getVersion());
		String repo = this.findMavenRepository(dep);
		if (this.getCheckAll() || (repo != null && repo.equals("releases"))) {
			res.setLatestVersion(this.getVersionsFromNexus(dep));
			res.checkIfNeedUpdate();
			if (this.getVerbose() || res.getShallBeUpdated()) {
				listResult.add(res);
			}
		}
	}

	private void checkParentDependency(List<DependencyCheckResult> listResult) {
		MavenProject parent = project.getParent();
		Dependency dep = new Dependency();
		dep.setArtifactId(parent.getArtifactId());
		dep.setGroupId(parent.getGroupId());
		dep.setVersion(parent.getVersion());
		dep.setType("parent");
		treatDependency(listResult, dep);
	}

	private void showListDependencyResults(List<DependencyCheckResult> listResult) {
		Integer longestGroup = getLongestGroup(listResult);
		for (DependencyCheckResult dcr : listResult) {
			System.out.format("%" + (longestGroup + 5) + "s%30s%15s%15s%15s%10s\n", dcr.getGroup(), dcr.getArtifact(), 
					dcr.getType(), dcr.getVersion(), dcr.getLatestVersion(), dcr.shallBeUpdatedRendered());
		}
		if (listResult.isEmpty()) {
			System.out.println("Check dependencies :: All the dependencies are up to date.");
		}
	}

	private Integer getLongestGroup(List<DependencyCheckResult> listResult) {
		Integer res = 0;
		for (DependencyCheckResult dcr : listResult) {
			if (dcr.getGroup().length()>res) {
				res = dcr.getGroup().length();
			}
		}
		return res;
	}

	private String getVersionsFromNexus(Dependency dep) {
		String url = nexusURL + "lucene/search?g=" + "&g=" + dep.getGroupId() + "&a=" + dep.getArtifactId() +  "&collapseresults=true";
		String content = readFileContent(url);
		Pattern pat = Pattern.compile("<latestRelease>(.*)?</latestRelease>");
		Matcher mat = pat.matcher(content);
		if (mat.find()) {
			return mat.group(1);
		}
		return null;
	}
	
	private String findMavenRepository(Dependency dep) {
		String url = nexusURL + "lucene/search?g=" + dep.getGroupId() + "&a=" + dep.getArtifactId() + "&collapseresults=true";
		String content = readFileContent(url);
		Pattern pat = Pattern.compile("<repositoryId>(.*)?</repositoryId>");
		Matcher mat = pat.matcher(content);
		if (mat.find()) {
			return mat.group(1);
		}
		return null;
	}

	private String readFileContent(String url){
		String res = "";
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(new URL(url).openStream()));
			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				res = res + inputLine + "\n";
			}
			in.close();
		}
		catch(Exception e) {
			System.out.println("Problem to read inputStream : " + url);
		}
		
		return res;
	}

	public Boolean getVerbose() {
		if (verbose == null) {
			verbose = false;
		}
		return verbose;
	}

	public void setVerbose(Boolean verbose) {
		this.verbose = verbose;
	}

	public Boolean getCheckAll() {
		if (checkAll == null) {
			return false;
		}
		return checkAll;
	}

	public void setCheckAll(Boolean checkAll) {
		this.checkAll = checkAll;
	}

	public Boolean getCheckDM() {
		if (checkDM == null) {
			checkDM = false;
		}
		return checkDM;
	}

	public void setCheckDM(Boolean checkDM) {
		this.checkDM = checkDM;
	}


}
