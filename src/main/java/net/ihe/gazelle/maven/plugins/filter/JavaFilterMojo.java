package net.ihe.gazelle.maven.plugins.filter;

import java.io.File;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;

/**
 * Filters Java resources, like .tpl2java files
 * 
 * @goal java-filter
 * @threadSafe
 */
public class JavaFilterMojo extends AbstractFilterMojo {

	@Override
	public void execute() throws MojoExecutionException, MojoFailureException {
		File sourceFolder = new File(project.getBasedir(), "src/main/java");
		File targetFolder = new File(project.getBuild().getDirectory(), "filtered-sources");

		filterFolder(sourceFolder, targetFolder, "tpl2java", "java");

		getLog().info("Adding source folder " + targetFolder.getPath());
		project.addCompileSourceRoot(targetFolder.getPath());
	}

}
