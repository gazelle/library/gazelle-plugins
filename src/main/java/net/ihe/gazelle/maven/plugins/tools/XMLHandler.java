package net.ihe.gazelle.maven.plugins.tools;

import java.io.File;

public interface XMLHandler {

	void file(File f);

	String text(int line, int col, String text);

	void startElement(String tagName);

	String attribute(int line, int col, String tagName, String lvalue, String rvalue);

	void endElement(String tagName);

	String getLabel();

}
