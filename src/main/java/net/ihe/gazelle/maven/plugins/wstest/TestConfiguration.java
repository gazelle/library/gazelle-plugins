package net.ihe.gazelle.maven.plugins.wstest;

/**
 * This class describes a test configuration:
 * - path to the soapui-project file to execute
 * - endpoint url (if different from the one defined in the project)
 * @author aberge
 *
 */

public class TestConfiguration {

	/**
	 * service endpoint to use (eg. localhost:8080)
	 * 
	 * @parameter
	 * @optional
	 */
	private String serviceEndpoint;
	
	/**
	 * paths to the soapui-project.xml files to execute (relative to project base directory)
	 * 
	 * @parameter
	 * @optional
	 */
	private String[] soapUIProjects;
	
	
	/**
	 * path to the directory containing the soapui-project.xml files to execute (relative to project base directory)
	 * 
	 * @parameter
	 * @optional
	 */
	private String soapUIProjectDirectory;
	

	public String[] getSoapUIProjects() {
		return soapUIProjects;
	}


	public String getServiceEndpoint() {
		return serviceEndpoint;
	}


	public String getSoapUIProjectDirectory() {
		return soapUIProjectDirectory;
	}

}
