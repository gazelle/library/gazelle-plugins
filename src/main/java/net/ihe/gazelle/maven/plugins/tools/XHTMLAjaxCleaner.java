package net.ihe.gazelle.maven.plugins.tools;

import java.io.File;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;

/**
 * Clean ajax attributes
 * 
 * @goal ajax-cleaner
 * @threadSafe
 */
public class XHTMLAjaxCleaner extends AbstractMojo {

	/**
	 * The directory where the XHTML can be found.
	 * 
	 * @parameter expression="${project.basedir}/src/main/webapp"
	 * @required
	 */
	private File xhtmlInputDirectory;

	@Override
	public void execute() throws MojoExecutionException, MojoFailureException {
		XMLHandlerAjaxCleaner xmlHandlerAjaxCleaner = new XMLHandlerAjaxCleaner();
		try {
			XMLParser.processXHTMLInFolder(xhtmlInputDirectory, xmlHandlerAjaxCleaner, true);
		} catch (Exception e) {
			throw new MojoFailureException("Failed to fix attributes", e);
		}
	}

}
