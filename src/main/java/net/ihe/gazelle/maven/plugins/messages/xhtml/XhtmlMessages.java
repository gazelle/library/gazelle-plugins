package net.ihe.gazelle.maven.plugins.messages.xhtml;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import net.ihe.gazelle.maven.plugins.tools.XMLHandler;
import net.ihe.gazelle.maven.plugins.tools.XMLParser;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.project.MavenProject;

/**
 * Translate XHTML files. Files in src/main/webapp are modified. A new xml file
 * is created in src/main/messages. Goals generate-messages and
 * aggregate-messages should be executed before
 *
 * @goal xhtml-messages
 * @phase process-resources
 * @threadSafe
 */
public class XhtmlMessages extends AbstractMojo {

    public static String[] DEFAULT_IGNORED_PARAM_NAMES = {"id", "sectionToReRender", "panelId", "panelsToReRender",
            "SuffixForID", "panel2refresh", "required", "filterId", "filterForm", "divIdVar", "dataListIdVar",
            "editorIdVar", "containerPanelId", "varList", "varTableId", "varColor", "tableName", "onchange_callback",
            "save_callback", "convert_urls", "theme_advanced_statusbar_location", "theme_advanced_resizing",
            "theme_advanced_toolbar_location", "theme_advanced_toolbar_align", "theme_advanced_buttons1",
            "theme_advanced_buttons2", "dataScrollerId", "dataTableId", "idToRerender", "beanName", "multiple",
            "extensions", "popup_id", "panel_id", "styleClass", "headerStyleClass", "bodyStyleClass",
            "footerStyleClass", "openByDefault", "icon_class", "dt_styleClass", "dd_styleClass"};

    public static List<String> getAllIgnoredParamNames(Log log, List<String> ignoredParamNamesUser) {
        List<String> ignoredParamNamesAll = new ArrayList<String>();
        for (String ignoredParamName : DEFAULT_IGNORED_PARAM_NAMES) {
            ignoredParamNamesAll.add(ignoredParamName);
        }
        if (ignoredParamNamesUser != null) {
            ignoredParamNamesAll.addAll(ignoredParamNamesUser);
        }
        String ignoredNames = Arrays.toString(ignoredParamNamesAll.toArray(new String[ignoredParamNamesAll.size()]));
        log.info("Ignoring ui:param/f:param with name in " + ignoredNames);
        log.info("Names can be added using ignoredParamNames in gazelle-plugins configuration. "
                + "(see mvn gazelle:help -Ddetail=true -Dgoal=check-xhtml-messages)");

        return ignoredParamNamesAll;
    }

    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

    /**
     * The current Maven project
     *
     * @parameter expression="${project}"
     * @readonly
     * @required
     */
    private MavenProject project;

    /**
     * The directory where the XHTML can be found.
     *
     * @parameter expression="${project.basedir}/src/main/webapp"
     * @required
     */
    private File xhtmlInputDirectory;

    /**
     * The directory where the generated resource files will be stored. The
     * directory will be registered as a resource root of the project such that
     * the generated files will participate in later build phases like packaing.
     *
     * @parameter expression= "${project.basedir}/src/main/messages"
     * @required
     */
    private File outputFolderMessages;

    /**
     * Key prefix used in the xml messages generated
     *
     * @parameter expression= "${keyPrefix}"
     * @required
     */
    private String keyPrefix;

    /**
     * Do not modify any xhtml file
     *
     * @parameter expression= "${simulate}"
     */
    private boolean simulate = false;

    /**
     * The directory where the generated resource files will be stored. The
     * directory will be registered as a resource root of the project such that
     * the generated files will participate in later build phases like packaing.
     *
     * @parameter expression=
     * "${project.build.directory}/generated-resources/messages-aggregated/messages_en.properties"
     * @required
     */
    private File existingMessages;

    /**
     * The list of ignored names in ui:param/f:param elements. value attribute
     * for this element will not be translated
     *
     * @parameter expression= "${ignoredParamNames}"
     */
    private List<String> ignoredParamNames;

    private Map<String, String> translations;
    private Map<String, String> reverseTranslations;
    private Map<String, String> relaxedTranslations;
    private Map<String, String> newTranslations;

    private boolean isCrowdin;

    public static void main(String[] args) throws MojoExecutionException, MojoFailureException {
        XhtmlMessages xhtmlMessages = new XhtmlMessages();
        xhtmlMessages.xhtmlInputDirectory = new File(
                "/home/glandais/code/maven/gazelle-modules/gazelle-common-war/src/main/webapp");
        xhtmlMessages.keyPrefix = "toto";
        xhtmlMessages.existingMessages = new File(
                "/home/glandais/code/maven/gazelle-modules/gazelle-common-war/target/generated-resources/messages-aggregated/messages_en.properties");
        xhtmlMessages.execute();
    }

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        Object messagesMode = project.getProperties().get("messages.mode");
        if ("crowdin".equals(messagesMode)) {
            isCrowdin = true;
        } else {
            isCrowdin = false;
        }

        if (!existingMessages.exists()) {
            getLog().info(existingMessages.getAbsolutePath() + " does not exist");
        } else {
            Set<SpotAttribute> spots = collectSpots();
            prepareTranslations();
            translateFiles(spots);
            createNewMessages();
        }
    }

    private void translateFiles(Set<SpotAttribute> spots) throws MojoFailureException {

        List<String> ignoredParamNamesAll = getAllIgnoredParamNames(getLog(), ignoredParamNames);

        XMLHandler translateHandler = new XMLHandlerTranslator(keyPrefix, spots, translations, reverseTranslations,
                relaxedTranslations, newTranslations, ignoredParamNamesAll);
        try {
            XMLParser.processXHTMLInFolder(xhtmlInputDirectory, translateHandler, !simulate);
        } catch (Exception e) {
            throw new MojoFailureException("Failed to translate", e);
        }
    }

    private Set<SpotAttribute> collectSpots() throws MojoFailureException {
        Set<SpotAttribute> spots = new HashSet<SpotAttribute>();
        XMLHandlerSpotFinder translateHandler = new XMLHandlerSpotFinder(spots);
        try {
            XMLParser.processXHTMLInFolder(xhtmlInputDirectory, translateHandler, false);
        } catch (Exception e) {
            throw new MojoFailureException("Failed to get message locations", e);
        }
        for (SpotAttribute spot : translateHandler.getNewSpots()) {
            getLog().info("Found spot : " + spot.toString());
        }
        return spots;
    }

    private void createNewMessages() throws MojoFailureException {
        if (isCrowdin) {
            createNewMessagesProperties();
        } else {
            createNewMessagesXml();
        }
    }

    private void createNewMessagesProperties() throws MojoFailureException {
        Properties newProperties = new Properties();
        newProperties.putAll(newTranslations);

        String fileName = "messages" + sdf.format(new Date()) + ".properties";
        try {
            FileOutputStream out = new FileOutputStream(new File(outputFolderMessages, fileName));
            newProperties.store(out, "Messages generated with automatic translation");
            out.close();
        } catch (Exception e) {
            throw new MojoFailureException("Failed to create messages properties", e);
        }

        getLog().info("A new messages file has been created (" + fileName + ")");
    }

    private void createNewMessagesXml() throws MojoFailureException {
        // Save messages.xml file
        Set<Entry<String, String>> entrySet = newTranslations.entrySet();
        StringBuilder sb = new StringBuilder();
        sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
        sb.append("<entries>\n");
        for (Entry<String, String> entry : entrySet) {
            sb.append("<entry>\r");
            sb.append("  <keyword>" + entry.getKey() + "</keyword>\r");
            sb.append("  <text xml:lang=\"en\">" + StringEscapeUtils.escapeXml(entry.getValue()) + "</text>\r");
            sb.append("</entry>\r");
        }
        sb.append("</entries>");
        String fileName = "messages" + sdf.format(new Date()) + ".xml";
        try {
            FileUtils.writeStringToFile(new File(outputFolderMessages, fileName), sb.toString(), "UTF-8");
        } catch (IOException e) {
            throw new MojoFailureException("Failed to create messages.xml", e);
        }
        getLog().info("A new messages file has been created (" + fileName + ")");
    }

    private void prepareTranslations() throws MojoFailureException {
        Properties messages = new Properties();
        try {
            messages.load(new FileInputStream(existingMessages));
        } catch (Exception e) {
            throw new MojoFailureException("Failed to read existing messages", e);
        }
        translations = new HashMap<String, String>();
        safePutAll(translations, messages);

        relaxedTranslations = new HashMap<String, String>();

        reverseTranslations = new HashMap<String, String>();
        Set<String> keySet = translations.keySet();
        for (String key : keySet) {
            String translation = translations.get(key);

            String relaxedTranslation = XMLHandlerTranslator.cleanExpression(translation);

            if (reverseTranslations.containsKey(translation)) {
                getLog().warn(key + " == " + reverseTranslations.get(translation) + " (" + translation + ")");
            } else {
                if (reverseTranslations.containsKey(relaxedTranslation)) {
                    getLog().warn(key + " ~= " + reverseTranslations.get(relaxedTranslation) + " (" + translation + ")");
                }
            }
            reverseTranslations.put(translation, key);
            relaxedTranslations.put(relaxedTranslation, key);
        }

        newTranslations = new HashMap<String, String>();
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    private void safePutAll(Map<String, String> target, Properties messages) {
        target.putAll((Hashtable) messages);
    }

}
