package net.ihe.gazelle.maven.plugins.filter;

import java.io.File;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;

/**
 * Filters webapp resources, like .tpl2 files
 * 
 * @goal webapp-filter
 * @threadSafe
 */
public class WebappFilterMojo extends AbstractFilterMojo {

	@Override
	public void execute() throws MojoExecutionException, MojoFailureException {
		File sourceFolder = new File(project.getBasedir(), "src/main/webapp");
		File targetFolder = new File(project.getBuild().getDirectory(), "filtered-webapp");

		filterFolder(sourceFolder, targetFolder, ".tpl2", "");
	}

}
