package net.ihe.gazelle.maven.plugins.hapi;

/**
 * Definition of a package object
 * @author aberge
 *
 */
public class Package {
	/**
	 * The name to give to the package
	 * @required
	 */
	private String name;
	/**
	 * URI of the HL7 Message profile
	 * @required
	 */
	private String[] profilesURI;
	/**
	 * list of concerns profiles OID
	 * @required
	 */
	private String[] profilesOID;
	
	public String[] getProfilesURI()
	{
		return this.profilesURI;
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public String[] getProfilesOID()
	{
		return this.profilesOID;
	}
}