package net.ihe.gazelle.maven.plugins.ramtarget;

import java.io.File;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.project.MavenProject;

/**
 * Reset target on the RAM drive
 * 
 * @goal delete-ramtarget
 * @threadSafe
 */
public class DeleteRamTargetMojo extends AbstractRamTargetMojo {

	/**
	 * The current Maven project
	 * 
	 * @parameter expression="${project}"
	 * @readonly
	 * @required
	 */
	private MavenProject project;

	/**
	 * Ram disk root folder
	 * 
	 * @parameter
	 * @required
	 */
	private String ramRootFolder;

	/**
	 * The directory where the generated resource files will be stored. The
	 * directory will be registered as a resource root of the project such that
	 * the generated files will participate in later build phases like packaing.
	 * 
	 * @parameter expression="${project.build.directory}"
	 * @required
	 */
	private File targetDirectory;

	@Override
	public void execute() throws MojoExecutionException, MojoFailureException {
		deleteFolder(targetDirectory);
		deleteFolder(getRamFolder(project, ramRootFolder));
	}

}
