package net.ihe.gazelle.maven.plugins.checkdependencies;

import java.util.List;
import java.util.Locale;

import org.apache.maven.doxia.sink.Sink;
import org.apache.maven.doxia.siterenderer.Renderer;
import org.apache.maven.project.MavenProject;
import org.apache.maven.reporting.AbstractMavenReport;
import org.apache.maven.reporting.MavenReportException;

/**
 * Write a report about checking dependencies
 *
 * @author abderrazek boufahja
 * @goal check-dependencies-report
 * @phase site
 */
public class CheckDependenciesReport extends AbstractMavenReport {
	

    /**
     * @parameter expression="${project}"
     * @required
     * @readonly
     */
	private MavenProject project;
	
	/**
     * @component
     */
	private Renderer siteRenderer;
	
	@Override
	public MavenProject getProject() {
		return project;
	}

	@Override
	public Renderer getSiteRenderer() {
		return siteRenderer;
	}

	public void setSiteRenderer(Renderer siteRenderer) {
		this.siteRenderer = siteRenderer;
	}

	public void setProject(MavenProject project) {
		this.project = project;
	}

	@Override
	public String getDescription(Locale arg0) {
		return "This module check if there are recent jar version comparing to used ones.";
	}

	@Override
	public String getName(Locale arg0) {
		return "Check Dependencies";
	}

	@Override
	public String getOutputName() {
		 return "checkdependencies";
	}

	@Override
	protected void executeReport(Locale locale) throws MavenReportException {
		CheckDependenciesMojo cd = new CheckDependenciesMojo();
		cd.setCheckAll(true);
		cd.setCheckDM(true);
		cd.setProject(project);
		cd.setVerbose(true);
		List<DependencyCheckResult> ldr = cd.extractListDependenciesCheckResult();
        final Sink sink = getSink();
        sink.head();
        sink.title();
        sink.text("Check Dependencies Report");
        sink.title_();
        sink.head_();
        sink.body();
        sink.section1();
		sink.sectionTitle1();
        sink.text("List of all dependencies jar");
        sink.sectionTitle1_();
        sink.section2();
        sink.sectionTitle2();
        sink.text("List of all dependencies jar coming from IHE");
        sink.sectionTitle2_();
        showListDependencyResultsFromIHE(ldr, sink);
        sink.section2_();
        
        sink.section2();
        sink.sectionTitle2();
        sink.text("List of all dependencies not from IHE jars");
        sink.sectionTitle2_();
        showListDependencyResultsNotFromIHE(ldr, sink);
        sink.section2_();
        
        sink.section1_();
        sink.body_();

        sink.flush();
        sink.close();
	}
	
	private void showListDependencyResultsNotFromIHE(List<DependencyCheckResult> listResult, Sink sink) {
		
		beginTable(sink);
		for (DependencyCheckResult dcr : listResult) {
			if (!dcr.getGroup().contains("ihe")) {
				sink.tableRow();
				sink.tableCell();
				if ("yes".equals(dcr.shallBeUpdatedRendered())) {
					sink.figure();
					sink.figureGraphics("images/icon_warning_sml.gif");
					sink.figure_();
				}
				else {
					sink.figure();
					sink.figureGraphics("images/icon_success_sml.gif");
					sink.figure_();
				}
				sink.tableCell_();
				sink.tableCell();
				sink.text(dcr.getGroup());
				sink.tableCell_();
				sink.tableCell();
				sink.text(dcr.getArtifact());
				sink.tableCell_();
				sink.tableCell();
				sink.text(dcr.getType());
				sink.tableCell_();
				sink.tableCell();
				sink.text(dcr.getVersion());
				sink.tableCell_();
				sink.tableCell();
				sink.text(dcr.getLatestVersion());
				sink.tableCell_();
				sink.tableRow_();
			}
		}
		sink.table_();
	}
	
	private void showListDependencyResultsFromIHE(List<DependencyCheckResult> listResult, Sink sink) {
		
		beginTable(sink);
		for (DependencyCheckResult dcr : listResult) {
			if (dcr.getGroup().contains("ihe")) {
				sink.tableRow();
				sink.tableCell();
				if ("yes".equals(dcr.shallBeUpdatedRendered())) {
					sink.figure();
					sink.figureGraphics("images/icon_error_sml.gif");
					sink.figure_();
				}
				else {
					sink.figure();
					sink.figureGraphics("images/icon_success_sml.gif");
					sink.figure_();
				}
				sink.tableCell_();
				sink.tableCell();
				sink.text(dcr.getGroup());
				sink.tableCell_();
				sink.tableCell();
				sink.text(dcr.getArtifact());
				sink.tableCell_();
				sink.tableCell();
				sink.text(dcr.getType());
				sink.tableCell_();
				sink.tableCell();
				sink.text(dcr.getVersion());
				sink.tableCell_();
				sink.tableCell();
				sink.text(dcr.getLatestVersion());
				sink.tableCell_();
				sink.tableRow_();
			}
		}
		sink.table_();
	}

	private void beginTable(Sink sink) {
		sink.table();
		sink.tableRow();
		sink.tableHeaderCell();
		sink.text("status");
		sink.tableHeaderCell_();
		sink.tableHeaderCell();
		sink.text("group");
		sink.tableHeaderCell_();
		sink.tableHeaderCell();
		sink.text("artifact)");
		sink.tableHeaderCell_();
		sink.tableHeaderCell();
		sink.text("type");
		sink.tableHeaderCell_();
		sink.tableHeaderCell();
		sink.text("version");
		sink.tableHeaderCell_();
		sink.tableHeaderCell();
		sink.text("latest version");
		sink.tableHeaderCell_();
		sink.tableRow_();
	}

	@Override
	protected String getOutputDirectory() {
		return null;
	}

}
