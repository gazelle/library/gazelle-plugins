package net.ihe.gazelle.maven.plugins.sql;

import java.io.File;
import java.util.Collection;
import java.util.List;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.handler.ArtifactHandler;
import org.apache.maven.artifact.metadata.ArtifactMetadata;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.artifact.resolver.filter.ArtifactFilter;
import org.apache.maven.artifact.versioning.ArtifactVersion;
import org.apache.maven.artifact.versioning.OverConstrainedVersionException;
import org.apache.maven.artifact.versioning.VersionRange;

public class SpecialArtifact implements Artifact {

	private Artifact delegator;

	public SpecialArtifact(Artifact delegator) {
		super();
		this.delegator = delegator;
	}

	public String getGroupId() {
		return delegator.getGroupId();
	}

	public String getArtifactId() {
		return delegator.getArtifactId();
	}

	public String getVersion() {
		return delegator.getVersion();
	}

	public void setVersion(String version) {
		delegator.setVersion(version);
	}

	public String getScope() {
		return delegator.getScope();
	}

	public String getType() {
		return delegator.getType();
	}

	public String getClassifier() {
		return delegator.getClassifier();
	}

	public boolean hasClassifier() {
		return delegator.hasClassifier();
	}

	public File getFile() {
		return delegator.getFile();
	}

	public void setFile(File destination) {
		delegator.setFile(destination);
	}

	public String getBaseVersion() {
		return delegator.getBaseVersion();
	}

	public void setBaseVersion(String baseVersion) {
		delegator.setBaseVersion(baseVersion);
	}

	public String getId() {
		return delegator.getId();
	}

	public String getDependencyConflictId() {
		return delegator.getDependencyConflictId();
	}

	public void addMetadata(ArtifactMetadata metadata) {
		delegator.addMetadata(metadata);
	}

	public Collection getMetadataList() {
		return delegator.getMetadataList();
	}

	public void setRepository(ArtifactRepository remoteRepository) {
		delegator.setRepository(remoteRepository);
	}

	public ArtifactRepository getRepository() {
		return delegator.getRepository();
	}

	public void updateVersion(String version, ArtifactRepository localRepository) {
		delegator.updateVersion(version, localRepository);
	}

	public String getDownloadUrl() {
		return delegator.getDownloadUrl();
	}

	public void setDownloadUrl(String downloadUrl) {
		delegator.setDownloadUrl(downloadUrl);
	}

	public ArtifactFilter getDependencyFilter() {
		return delegator.getDependencyFilter();
	}

	public void setDependencyFilter(ArtifactFilter artifactFilter) {
		delegator.setDependencyFilter(artifactFilter);
	}

	public ArtifactHandler getArtifactHandler() {
		return delegator.getArtifactHandler();
	}

	public List getDependencyTrail() {
		return delegator.getDependencyTrail();
	}

	public void setDependencyTrail(List dependencyTrail) {
		delegator.setDependencyTrail(dependencyTrail);
	}

	public int compareTo(Object o) {
		return delegator.compareTo(o);
	}

	public void setScope(String scope) {
		delegator.setScope(scope);
	}

	public VersionRange getVersionRange() {
		return delegator.getVersionRange();
	}

	public void setVersionRange(VersionRange newRange) {
		delegator.setVersionRange(newRange);
	}

	public void selectVersion(String version) {
		delegator.selectVersion(version);
	}

	public void setGroupId(String groupId) {
		delegator.setGroupId(groupId);
	}

	public void setArtifactId(String artifactId) {
		delegator.setArtifactId(artifactId);
	}

	public boolean isSnapshot() {
		return delegator.isSnapshot();
	}

	public void setResolved(boolean resolved) {
		delegator.setResolved(resolved);
	}

	public boolean isResolved() {
		return delegator.isResolved();
	}

	public void setResolvedVersion(String version) {
		delegator.setResolvedVersion(version);
	}

	public void setArtifactHandler(ArtifactHandler handler) {
		delegator.setArtifactHandler(handler);
	}

	public boolean isRelease() {
		return delegator.isRelease();
	}

	public void setRelease(boolean release) {
		delegator.setRelease(release);
	}

	public List getAvailableVersions() {
		return delegator.getAvailableVersions();
	}

	public void setAvailableVersions(List versions) {
		delegator.setAvailableVersions(versions);
	}

	public boolean isOptional() {
		return delegator.isOptional();
	}

	public void setOptional(boolean optional) {
		delegator.setOptional(optional);
	}

	public ArtifactVersion getSelectedVersion() throws OverConstrainedVersionException {
		return delegator.getSelectedVersion();
	}

	public boolean isSelectedVersionKnown() throws OverConstrainedVersionException {
		return delegator.isSelectedVersionKnown();
	}

	public int hashCode() {
		int result = 17;
		result = 37 * result + getGroupId().hashCode();
		result = 37 * result + getArtifactId().hashCode();
		return result;
	}

	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		if (!(o instanceof Artifact)) {
			return false;
		}
		Artifact a = (Artifact) o;
		if (!a.getGroupId().equals(getGroupId())) {
			return false;
		} else if (!a.getArtifactId().equals(getArtifactId())) {
			return false;
		}
		// We don't consider the version range in the comparison, just the
		// resolved version
		return true;
	}

	@Override
	public String toString() {
		return getGroupId() + ":" + getArtifactId();
	}
}
