package net.ihe.gazelle.maven.plugins.ramtarget;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.project.MavenProject;

public abstract class AbstractRamTargetMojo extends AbstractMojo {

	protected File getRamFolder(MavenProject project, String ramRootFolder) {
		String folder = Integer.toHexString(project.getBasedir().hashCode()) + "_" + project.getGroupId() + "_"
				+ project.getArtifactId();
		File ramFolder = new File(ramRootFolder, folder);
		return ramFolder;
	}

	protected void deleteFolder(File folderToDelete) throws MojoFailureException {
		try {
			getLog().info("Deleting " + folderToDelete.getAbsolutePath());
			final Process processRm = Runtime.getRuntime().exec(
					new String[] { "rm", "-Rf", folderToDelete.getAbsolutePath() });
			createLogThreads(processRm);
			processRm.waitFor();
			processRm.destroy();
		} catch (IOException e) {
			throw new MojoFailureException("Failed to delete folder " + folderToDelete.getAbsolutePath());
		} catch (InterruptedException e) {
			throw new MojoFailureException("Failed to delete folder " + folderToDelete.getAbsolutePath());
		}
	}

	protected void createLogThreads(final Process process) {
		new Thread() {
			public void run() {
				try {
					BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
					String line = "";
					try {
						while ((line = reader.readLine()) != null) {
							getLog().info(line);
						}
					} finally {
						reader.close();
					}
				} catch (IOException ioe) {
					getLog().error("", ioe);
				}
			}
		}.start();

		new Thread() {
			public void run() {
				try {
					BufferedReader reader = new BufferedReader(new InputStreamReader(process.getErrorStream()));
					String line = "";
					try {
						while ((line = reader.readLine()) != null) {
							getLog().error(line);
						}
					} finally {
						reader.close();
					}
				} catch (IOException ioe) {
					getLog().error("", ioe);
				}
			}
		}.start();
	}
}
