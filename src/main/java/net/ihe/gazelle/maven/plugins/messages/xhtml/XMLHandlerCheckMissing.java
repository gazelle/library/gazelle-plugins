package net.ihe.gazelle.maven.plugins.messages.xhtml;

import net.ihe.gazelle.maven.plugins.tools.XMLHandler;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.maven.plugin.logging.Log;

import java.io.File;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class XMLHandlerCheckMissing implements XMLHandler {

    private static final Pattern PATTERN_SPACES = Pattern.compile("\\s+");

    private static String STRIP_CHARS;

    static {
        StringBuilder stripChars = new StringBuilder();
        // control chars
        for (int i = 0; i < 33; i++) {
            stripChars.append((char) i);
        }
        stripChars.append("!#*+,-./:;<=>?_|~\\");
        // incorrect chars
        for (int i = 169; i < 256; i++) {
            stripChars.append((char) i);
        }
        STRIP_CHARS = stripChars.toString();
    }

    private Set<SpotAttribute> spots;
    private Stack<String> stack;

    private List<String> ignoredParamNames;
    private boolean ignoreNextAttributes = false;

    private File f;

    private Log log;

    private boolean hasMissing = false;

    public XMLHandlerCheckMissing(Set<SpotAttribute> spots, Log log, List<String> ignoredParamNames) {
        super();
        this.stack = new Stack<String>();
        this.spots = spots;
        this.log = log;
        this.ignoredParamNames = ignoredParamNames;
    }

    @Override
    public String text(int line, int col, String text) {
        String tagName = "";
        if (!stack.empty()) {
            tagName = stack.peek();
            if (tagSupported(tagName)) {
                return processText(line, col, text, "");
            }
        }
        return null;
    }

    private boolean tagSupported(String tagName) {
        if (tagName.equals("script")) {
            return false;
        }
        if (tagName.equals("style")) {
            return false;
        }
        return true;
    }

    private boolean attributeSupported(String tagName, String attributeName) {
        if (ignoreNextAttributes) {
            return false;
        }
        for (SpotAttribute spot : spots) {
            if (spot.supportsTextFromAttribute(tagName, attributeName)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void startElement(String tagName) {
        ignoreNextAttributes = false;
        stack.push(tagName);
    }

    @Override
    public void endElement(String tagName) {
        stack.pop();
    }

    public boolean hasMissing() {
        return hasMissing;
    }

    @Override
    public String attribute(int line, int col, String tagName, String lvalue, String rvalue) {
        if ((tagName.equals("ui:param") || tagName.equals("f:param")) && lvalue.equals("name")) {
            if (ignoredParamNames != null && ignoredParamNames.contains(rvalue)) {
                ignoreNextAttributes = true;
            }
        }
        if (ignoreNextAttributes) {
            if (rvalue != null && rvalue.contains("messages[")) {
                log.warn("Strange translation in " + f.getName() + " (line:" + line + ",col:" + col + " - " + lvalue
                        + ") : " + rvalue);
            }
        } else if (attributeSupported(tagName, lvalue)) {
            return processText(line, col, rvalue, lvalue);
        }
        return null;
    }

    public String processText(int line, int col, String input, String attr) {
        Set<String> elExpressions = new HashSet<String>();
        elExpressions.addAll(elExpressions(input, "#{", "}"));
        elExpressions.addAll(elExpressions(input, "${", "}"));

        List<String> splittedInput = splitExpressions(input, elExpressions);

        for (String expression : splittedInput) {
            if (shouldProcess(expression)) {
                translateExpression(line, col, expression, attr);
            }
        }
        return null;
    }

    public static String cleanExpression(String expression) {
        String stripped = StringUtils.strip(expression, STRIP_CHARS);
        stripped = stripped.replaceAll("[\\t\\r\\n\\v\\f]", " ");
        Matcher matcher = PATTERN_SPACES.matcher(stripped);
        if (matcher.find()) {
            stripped = matcher.replaceAll(" ");
        }
        return stripped;
    }

    private void translateExpression(int line, int col, String expression, String attr) {
        String cleanExpression = expression;

        String stripped = StringUtils.strip(cleanExpression, STRIP_CHARS);
        if (stripped.length() != cleanExpression.length()) {
            cleanExpression = stripped;
        }

        cleanExpression = cleanExpression.replaceAll("[\\t\\r\\n\\v\\f]", " ");
        Matcher matcher = PATTERN_SPACES.matcher(cleanExpression);
        if (matcher.find()) {
            cleanExpression = matcher.replaceAll(" ");
        }
        cleanExpression = StringEscapeUtils.unescapeHtml(cleanExpression);
        String type = "text";
        if (!attr.isEmpty()) {
            type = "attribute: " + attr;
        }
        log.warn("Missing translation in " + f.getName() + " (line:" + line + ",col:" + col + ") " + type + " = " + cleanExpression);
        hasMissing = true;
    }

    public boolean shouldProcess(String text) {
        return text != null && StringUtils.indexOfAny(text.toLowerCase(), "azertyuiopmlkjhgfdsqwxcvbn") >= 0
                && !text.contains(".seam") && !text.contains(".xhtml") && !text.startsWith("#{")
                && !text.startsWith("${");
    }

    public Collection<? extends String> elExpressions(String text, String open, String close) {
        String[] elExpressions = StringUtils.substringsBetween(text, open, close);
        List<String> result = new ArrayList<String>();
        if (elExpressions != null) {
            for (String elExpression : elExpressions) {
                result.add(open + elExpression + close);
            }
        }
        return result;
    }

    public List<String> splitExpressions(String text, Set<String> elExpressions) {
        List<String> result = new ArrayList<String>();
        result.add(text);
        for (String elExpression : elExpressions) {
            List<String> tmpResult = new ArrayList<String>();
            for (String string : result) {
                tmpResult.addAll(splitExpression(string, elExpression));
            }
            result = tmpResult;
        }
        return result;
    }

    public static List<String> splitExpression(String text, String elExpression) {
        int startPos = StringUtils.indexOf(text, elExpression);
        ArrayList<String> result = new ArrayList<String>();

        if (startPos == 0) {
            if (text.length() == elExpression.length()) {

                result.add(text);
            } else {

                result.add(elExpression);

                String subText = text.substring(startPos + elExpression.length());
                result.addAll(splitExpression(subText, elExpression));
            }
        } else if (startPos >= 0) {

            if (startPos == text.length() - elExpression.length()) {

                String subText = text.substring(0, startPos);
                result.addAll(splitExpression(subText, elExpression));

                result.add(elExpression);
            } else {

                String subText = text.substring(0, startPos);
                result.addAll(splitExpression(subText, elExpression));

                result.add(elExpression);

                subText = text.substring(startPos + elExpression.length());
                result.addAll(splitExpression(subText, elExpression));
            }
        } else {
            result.add(text);
        }
        return result;
    }

    @Override
    public String getLabel() {
        return "Translating";
    }

    @Override
    public void file(File f) {
        this.f = f;
    }
}
