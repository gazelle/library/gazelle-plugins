package net.ihe.gazelle.maven.plugins.filter;

import java.io.File;
import java.util.List;

import org.apache.maven.execution.MavenSession;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.project.MavenProject;
import org.apache.maven.shared.filtering.MavenFileFilter;
import org.apache.maven.shared.filtering.MavenFilteringException;
import org.apache.maven.shared.filtering.MavenResourcesExecution;

public abstract class AbstractFilterMojo extends AbstractMojo {

	/**
	 * The current Maven project
	 * 
	 * @parameter expression="${project}"
	 * @readonly
	 * @required
	 */
	protected MavenProject project;

	/**
	 * @component role="org.apache.maven.shared.filtering.MavenFileFilter"
	 *            role-hint="default"
	 * @required
	 */
	protected MavenFileFilter mavenFileFilter;

	protected List filterWrappers;

	/**
	 * Expression preceded with the String won't be interpolated \${foo} will be
	 * replaced with ${foo}
	 * 
	 * @parameter expression="${maven.ear.escapeString}"
	 */
	protected String escapeString;

	/**
	 * Filters (property files) to include during the interpolation of the
	 * pom.xml.
	 * 
	 * @parameter
	 */
	protected List filters;

	/**
	 * To escape interpolated value with windows path c:\foo\bar will be
	 * replaced with c:\\foo\\bar
	 * 
	 * @parameter expression="${maven.ear.escapedBackslashesInFilePath}"
	 *            default-value="false"
	 */
	protected boolean escapedBackslashesInFilePath;

	/**
	 * @parameter expression="${session}"
	 * @readonly
	 * @required
	 */
	protected MavenSession session;

	protected void filterFolder(File sourceFolder, File targetFolder, String pattern, String patternReplace)
			throws MojoExecutionException {
		File[] files = sourceFolder.listFiles();
		for (File file : files) {
			if (!file.getName().startsWith(".")) {
				if (file.isDirectory()) {
					filterFolder(new File(sourceFolder, file.getName()), new File(targetFolder, file.getName()),
							pattern, patternReplace);
				} else {
					if (file.getName().contains(pattern)) {
						targetFolder.mkdirs();
						filter(file, new File(targetFolder, file.getName().replaceAll(pattern, patternReplace)));
					}
				}
			}
		}
	}

	protected void filter(File file, File target) throws MojoExecutionException {
		try {
			getLog().info("Transforming " + file.getPath());
			target.getParentFile().mkdirs();
			mavenFileFilter.copyFile(file, target, true, getFilterWrappers(), null);
		} catch (MavenFilteringException e) {
			throw new MojoExecutionException("Failed to filter", e);
		}
	}

	protected List getFilterWrappers() throws MojoExecutionException {
		if (filterWrappers == null) {
			try {
				MavenResourcesExecution mavenResourcesExecution = new MavenResourcesExecution();
				mavenResourcesExecution.setEscapeString(escapeString);
				filterWrappers = mavenFileFilter.getDefaultFilterWrappers(project, filters,
						escapedBackslashesInFilePath, this.session, mavenResourcesExecution);
			} catch (MavenFilteringException e) {
				getLog().error("fail to build filering wrappers " + e.getMessage());
				throw new MojoExecutionException(e.getMessage(), e);
			}
		}
		return filterWrappers;
	}

}
