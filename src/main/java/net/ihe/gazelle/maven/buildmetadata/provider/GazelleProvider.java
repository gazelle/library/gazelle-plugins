package net.ihe.gazelle.maven.buildmetadata.provider;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.TimeZone;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

import org.apache.commons.collections15.list.SetUniqueList;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.DefaultArtifact;
import org.apache.maven.artifact.handler.DefaultArtifactHandler;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.artifact.resolver.ArtifactNotFoundException;
import org.apache.maven.artifact.resolver.ArtifactResolutionException;
import org.apache.maven.artifact.resolver.ArtifactResolver;
import org.apache.maven.model.Contributor;
import org.apache.maven.model.Organization;
import org.apache.maven.model.Profile;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.component.repository.exception.ComponentLookupException;

import de.smartics.maven.plugin.buildmetadata.data.AbstractMetaDataProvider;

public class GazelleProvider extends AbstractMetaDataProvider {

	private static final String BUILD_CONTRIBUTORS = "build.contributors";
	private static final String BUILD_FULL_DATE = "build.date.full";
	private static final String BUILD_APPLICATION_NAME = "build.application.name";
	private static final String BUILD_PROFILE = "build.maven.execution.profiles.active";

	public void provideBuildMetaData(Properties properties) throws MojoExecutionException {
		List<String> organisations = SetUniqueList.decorate(new ArrayList<String>());

		Organization organization = project.getOrganization();
		if (organization != null) {
			String organizationName = organization.getName();
			if (organizationName != null) {
				organisations.add(organizationName);
			}
		}

		List<Contributor> developpers = project.getDevelopers();
		if (developpers != null) {
			addOrgaOfContribs(organisations, developpers);
		}
		List<Contributor> contributors = project.getContributors();
		if (contributors != null) {
			addOrgaOfContribs(organisations, contributors);
		}

		StringBuilder organisationsStr = new StringBuilder();
		for (String organisation : organisations) {
			if (organisationsStr.length() > 0) {
				organisationsStr.append(", ");
			}
			organisationsStr.append(organisation);
		}

		properties.setProperty(BUILD_CONTRIBUTORS, organisationsStr.toString());
		properties.setProperty(BUILD_APPLICATION_NAME, project.getName());
		List<Profile> activeProfiles;
		if (project.hasParent()) {
			activeProfiles = project.getParent().getExecutionProject().getActiveProfiles();
		} else {
			activeProfiles = project.getExecutionProject().getActiveProfiles();
		}

		StringBuilder profiles = new StringBuilder();
		for (Profile profile : activeProfiles) {
			if (profile != null) {
				if (profiles.length() > 0) {
					profiles.append(", ");
				}
				profiles.append(profile.getId());
			}

		}

		properties.setProperty(BUILD_PROFILE, profiles.toString());

		DateFormat formatter = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.LONG, Locale.getDefault());
		formatter.setTimeZone(TimeZone.getDefault());

		Calendar buildCalendar = Calendar.getInstance();
		String buildFullDate = formatter.format(buildCalendar.getTime());
		properties.setProperty(BUILD_FULL_DATE, buildFullDate);

		setParentProjectVersionAsProperty(project, properties);
	}

	private void setParentProjectVersionAsProperty(MavenProject currentProject, Properties properties)
			throws MojoExecutionException {
		if (currentProject.hasParent()) {
			MavenProject parentProject = currentProject.getParent();
			// project.getParent().getArtifact().getSelectedVersion().get
			String parentVersion = "";
			if (parentProject.getVersion().endsWith("SNAPSHOT")) {

				Artifact parentArtifact = parentProject.getArtifact();
				Artifact metadataArtifact = new DefaultArtifact(parentArtifact.getGroupId(),
						parentArtifact.getArtifactId(), parentArtifact.getVersionRange(), parentArtifact.getScope(),
						"zip", "metadata", new DefaultArtifactHandler("zip"));

				if (!metadataArtifact.isResolved()) {
					try {
						ArtifactResolver resolver = (ArtifactResolver) session.lookup(ArtifactResolver.ROLE);
						ArtifactRepository localRepository = session.getLocalRepository();
						List<?> remoteRepositories = project.getRemoteArtifactRepositories();
						resolver.resolve(metadataArtifact, remoteRepositories, localRepository);
					} catch (ArtifactResolutionException e) {
						throw new MojoExecutionException("Failed to find " + metadataArtifact, e);
					} catch (ArtifactNotFoundException e) {
						throw new MojoExecutionException("Failed to find " + metadataArtifact, e);
					} catch (ComponentLookupException e) {
						throw new MojoExecutionException("Failed to find " + metadataArtifact, e);
					}
				}
				File artifactFile = metadataArtifact.getFile();

				try {
					ZipFile zipFile = new ZipFile(artifactFile);
					Enumeration<? extends ZipEntry> entries = zipFile.entries();
					while (entries.hasMoreElements()) {
						ZipEntry zipEntry = entries.nextElement();

						if (!zipEntry.isDirectory()) {
							String zipEntryName = zipEntry.getName();
							int lastIndexOf = zipEntryName.lastIndexOf('/') + 1;
							String fileName = zipEntryName.substring(lastIndexOf);

							if (fileName.equals("build.properties")) {
								InputStream inputStream = zipFile.getInputStream(zipEntry);
								Properties parentProperties = new Properties();
								parentProperties.load(inputStream);

								Set<Entry<Object, Object>> entrySet = parentProperties.entrySet();
								for (Entry<Object, Object> entry : entrySet) {
									String key = (String) entry.getKey();
									String value = (String) entry.getValue();
									if (key.equals("build.version.full")) {
										String reference = parentProject.getGroupId() + ":"
												+ parentProject.getArtifactId() + ":" + value;
										properties.setProperty("build.parent.reference", reference);
									}
									if (key.startsWith("build.parent.")) {
										String newKey = key.replace("build.parent.", "build.parent.parent.");
										properties.setProperty(newKey, value);
									}
								}
							}
						}
					}
				} catch (ZipException e) {
					System.err.println("Unable to process artifact (not a valid zip file) : " + artifactFile);
				} catch (IOException e) {
					System.err.println("Unable to process artifact (not a valid zip file) : " + artifactFile);
				}
			} else {
				parentVersion = parentProject.getVersion();
				String reference = parentProject.getGroupId() + ":" + parentProject.getArtifactId() + ":"
						+ parentVersion;
				properties.setProperty("build.parent.reference", reference);
			}
		}
	}

	private void addOrgaOfContribs(List<String> organisations, List<Contributor> contributors) {
		for (Contributor contributor : contributors) {
			String organization = contributor.getOrganization();
			if (organization != null) {
				organisations.add(organization);
			}
		}
	}
}
